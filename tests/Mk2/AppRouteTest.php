<?php
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Slim\Factory\AppFactory;
use Slim\App;

//用來測試所有routes的撰寫是否正確
class AppRouteTest extends TestCase 
{
    public function testAppRoutes()
    {
        $app = AppFactory::create();
        $files = glob(__DIR__ . '/../../routes/public/' . '*.php');
        foreach ($files as $file) {
            require_once($file);
        }
        $this->assertInstanceOf(App::class, $app);
    }
}