<?php
use PHPUnit\Framework\TestCase;
use App\Utility\Utility;

require_once __DIR__ . "/../../vendor/autoload.php";

class UtilityTest extends TestCase 
{
    public function testGetDateTimeWithMicrosecond()
    {
        $test = Utility::getDateTimeWithMicrosecond();
        $this->assertMatchesRegularExpression('/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})\.\d{6}$/', $test);
    }

    public function testSafeCombineFilePath()
    {
        $basePath = '/var';

        // 註: 若要跑成功的case，執行環境必須實際存在該路徑。由於框架的執行環境各異有可能失敗，在此將成功的case註解
        // $testNormalPath = '/www/html';
        // $expectedPath = $basePath . $testNormalPath;
        // $fullPath1 = Utility::safeCombineFilePath($basePath, $testNormalPath);
        // $this->assertEquals($expectedPath, $fullPath1);

        $testTraversalPath = '/revision/share_od/../../../../../../../../etc/passwd';
        $fullPath2 = Utility::safeCombineFilePath($basePath, $testTraversalPath);
        $this->assertEquals(false, $fullPath2);

        $testTraversalPath = '/%2e%2e/etc/passwd';
        $fullPath3 = Utility::safeCombineFilePath($basePath, $testTraversalPath);
        $this->assertEquals(false, $fullPath3);

        $testTraversalPath = '/%252e%252e/etc/passwd';
        $fullPath3 = Utility::safeCombineFilePath($basePath, $testTraversalPath);
        $this->assertEquals(false, $fullPath3);
    }

    public function testFilterInputAsInteger()
    {
        //測文字整數
        $expected = 999999999;
        $test = '999999999';
        $result = Utility::filterInputAsInteger($test, 0);
        $this->assertEquals($expected, $result);

        //測超過32bit整數
        $expected = 2147483648;
        $test = 2147483648;
        $result = Utility::filterInputAsInteger($test, 0);
        $this->assertEquals($expected, $result);

        //測負整數
        $expected = -1;
        $test = -1;
        $result = Utility::filterInputAsInteger($test, 0);
        $this->assertEquals($expected, $result);

        //測非數字
        $expected = 0;
        $test = 'abcde';
        $result = Utility::filterInputAsInteger($test, $expected);
        $this->assertEquals($expected, $result);

        //測浮點數
        $expected = 100;
        $test = '99.999999124456767';
        $result = Utility::filterInputAsInteger($test, $expected);
        $this->assertEquals($expected, $result);
    }

    public function testGetClientIp()
    {
        $ip = Utility::get_client_ip();
        $this->assertIsString($ip);
        //Regexp from: https://stackoverflow.com/questions/5284147/validating-ipv4-addresses-with-regexp
        $this->assertMatchesRegularExpression('/^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$/', $ip);
    }

    public function testCheckIpInLimit()
    {
        $testIp = '192.168.2.10';
        $this->assertTrue(Utility::check_ip_in_limit($testIp, '192.168.2.8-192.168.2.11'));
        $this->assertTrue(Utility::check_ip_in_limit($testIp, '192.168.2.0-192.168.2.10'));
        $this->assertTrue(Utility::check_ip_in_limit($testIp, '192.168.2.10-192.168.2.254'));
        $this->assertTrue(Utility::check_ip_in_limit($testIp, '192.168.2.10-192.168.20.254'));
        $this->assertTrue(Utility::check_ip_in_limit($testIp, '192.168.2.1-15'));

        $this->assertFalse(Utility::check_ip_in_limit($testIp, '192.168.2.1-192.168.2.9'));
        $this->assertFalse(Utility::check_ip_in_limit($testIp, '192.168.2.11-192.168.2.19'));
        $this->assertFalse(Utility::check_ip_in_limit($testIp, '192.168.3.1-192.168.3.255'));
        $this->assertFalse(Utility::check_ip_in_limit($testIp, '192.168.1.1-192.168.2.9'));
        $this->assertFalse(Utility::check_ip_in_limit($testIp, '192.168.2.11-255'));

        $testIp = '127.0.0.1';
        $this->assertTrue(Utility::check_ip_in_limit($testIp, '127.0.0.1'));

        //Invalid ip case
        $testInvalidIp = '194.235.111';
        $this->assertFalse(Utility::check_ip_in_limit($testInvalidIp, '194.255.111.1'));
        $testInvalidIp = '257.235.111.1';
        $this->assertFalse(Utility::check_ip_in_limit($testInvalidIp, '194.255.111.1'));
    }

    public function testFormatBytes()
    {
        //精度為0
        $testSet = [
            '0 B' => 0,
            '128 B' => 128,
            '1 KB' => 1024,
            '12 MB' => 12582912,
            '25 MB' => (int)(25.05*1024*1024),
            '256 GB' => (int)(255.878*1024*1024*1024),
            '1023 GB' => 1023*1024*1024*1024,
            '1024 TB' => 1024*1024*1024*1024*1024,
        ];
        foreach ($testSet as $expect => $bytes) {
            $result = Utility::formatBytes($bytes, 0);
            $this->assertEquals($expect, $result);
        }

        //精度為2
        $testSet = [
            '0.00 B' => 0,
            '128.00 B' => 128,
            '1.00 KB' => 1024,
            '12.00 MB' => 12582912,
            '25.05 MB' => (int)(25.05*1024*1024),
            '255.88 GB' => (int)(255.878*1024*1024*1024),
            '1023.00 GB' => 1023*1024*1024*1024,
            '1.10 TB' => (int)(1.1*1024*1024*1024*1024),
            '1024.00 TB' => 1024*1024*1024*1024*1024,
        ];
        foreach ($testSet as $expect => $bytes) {
            $result = Utility::formatBytes($bytes, 2);
            $this->assertEquals($expect, $result);
        }
    }

    public function testGeneratorPassword()
    {
        $length = 12;
        $pw = Utility::generator_password($length);
        $this->assertIsString($pw);
        $this->assertEquals($length, strlen($pw));

        $length = 5;
        $keyspace = 'abcd1234';
        $pw = Utility::generator_password($length, $keyspace);
        $this->assertEquals($length, strlen($pw));
        $this->assertMatchesRegularExpression('/^[a,b,c,d,1,2,3,4]{5}$/', $pw);
    }

    public function testGenUUIDv4()
    {
        $uuid = Utility::gen_uuid_v4();
        $this->assertEquals(36, strlen($uuid));
        $this->assertMatchesRegularExpression('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i', $uuid);
    }

    public function testIsValidUUIDv4()
    {
        //測正確
        $testValidSet = [
            '24435595-4a76-468f-8411-9ed388afcca8',
            'a1e3fb0b-e5cd-4eab-b44d-d0f135bb71a6',
            'C40AC5CC-F7DF-4E41-84F9-2ED547D1DD66',
        ];
        foreach ($testValidSet as $uuid) {
            $this->assertTrue(Utility::isValidUUIDv4($uuid));
        }

        //測錯誤
        $testInvalidSet = [
            '',
            'abcde',
            '00000000-0000-0000-0000-000000000000',
            'a1e3fb0b-e5cd-4eab-0000-d0f135bb71a6',
        ];
        foreach ($testInvalidSet as $uuid) {
            $this->assertFalse(Utility::isValidUUIDv4($uuid));
        }
    }

    public function testConvertStandardDateTime()
    {
        $dateTimeObj = new DateTime('2022-12-31 10:20:30');
        $testSet = [
            '' => ['date_time' => null, 'format_type' => 'long'],
            '2023-02-28 13:10:30' => ['date_time' => '2023-02-28 13:10:30', 'format_type' => 'long'],
            '2023-02-28 13:10' => ['date_time' => '2023-02-28 13:10:30', 'format_type' => 'medium'],
            '2023-02-28' => ['date_time' => '2023-02-28 13:10:30', 'format_type' => 'short'],
            '2023' => ['date_time' => '2023-02-28 13:10:30', 'format_type' => 'year'],
            '02' => ['date_time' => '2023-02-28 13:10:30', 'format_type' => 'month'],
            '28' => ['date_time' => '2023-02-28 13:10:30', 'format_type' => 'day'],
            '2022-12-31 10:20:30' => ['date_time' => $dateTimeObj, 'format_type' => 'long'],
            '2022-12-31 10:20' => ['date_time' => $dateTimeObj, 'format_type' => 'medium'],
            '2022-12-31' => ['date_time' => $dateTimeObj, 'format_type' => 'short'],
            '2022' => ['date_time' => $dateTimeObj, 'format_type' => 'year'],
            '12' => ['date_time' => $dateTimeObj, 'format_type' => 'month'],
            '31' => ['date_time' => $dateTimeObj, 'format_type' => 'day'],
        ];
        foreach ($testSet as $expectDate => $set) {
            $this->assertEquals($expectDate, Utility::convertStandardDateTime($set['date_time'], $set['format_type']), 'Current set: ' . json_encode($set, JSON_UNESCAPED_UNICODE));
        }
    }

    public function testArrayToCsv()
    {
        //由於Utility::array_to_csv執行後會關閉resource,其實是無法測試的;
        //若要做unit test就需要先將原程式的fclose註解掉才能跑此測試,故註記跳過
        $this->markTestSkipped('若要test就需要先將Utility::array_to_csv()的fclose註解掉才能跑此測試,故註記跳過');
        $testData = [
            [
                'id' => 1,
                'title' => 'test1',
                'user' => 'BP Boy',
            ],[
                'id' => 2,
                'title' => 'test2',
                'user' => 'BP Girl',
            ],[
                'id' => 3,
                'title' => 'test3',
                'user' => 'BP Boss',
            ],
        ];
        $outBuffer = fopen("php://memory",'rw');
        Utility::array_to_csv($testData, $outBuffer);
        $this->assertIsResource($outBuffer);
        rewind($outBuffer);
        $header = fgets($outBuffer);
        $this->assertStringContainsString('id,title,user', $header);
        $line1 = fgets($outBuffer);
        $this->assertStringContainsString('test1', $line1);
        $line2 = fgets($outBuffer);
        $this->assertStringContainsString('test2', $line2);
        $line3 = fgets($outBuffer);
        $this->assertStringContainsString('test3', $line3);
        fclose($outBuffer);
    }

    public function testAssocArrayToCsv()
    {
        $testData = [
            [
                'id' => 1,
                'title' => 'test1',
                'user' => 'BP Boy',
            ],[
                'id' => 2,
                'title' => 'test2',
                'user' => 'BP Girl',
            ],[
                'id' => 3,
                'title' => 'test3',
                'user' => 'BP Boss',
            ],
        ];
        $delimiter  = ',';
        $result = Utility::assoc_array_to_csv($testData, $delimiter);
        $this->assertIsString($result);
        $resultLines = explode("\n", $result);
        $this->assertNotEmpty($resultLines);
        $this->assertStringContainsString('"id","title","user"', $resultLines[0]);
        $this->assertStringContainsString('test1', $resultLines[1]);
        $this->assertStringContainsString('test2', $resultLines[2]);
        $this->assertStringContainsString('BP Boss', $resultLines[3]);
    }

    public function testCheckRequired()
    {
        //Success case
        $testSuccessSet = [
            [
                'param' => ['field1' => 'a'],
                'required' => ['field1'],
            ],[
                'param' => ['field2' => ['array']],
                'required' => ['field2'],
            ],[
                'param' => ['field3' => 'a', 'field4' => 'b'],
                'required' => ['field3', 'field4'],
            ],
        ];
        foreach ($testSuccessSet as $set) {
            $result = Utility::checkRequired($set['param'], $set['required']);
            $this->assertTrue($result['success'], 'Current set: ' . json_encode($set));
        }

        //Fail case
        $testFailSet = [
            [
                'param' => [],
                'required' => ['field_none'],
            ],[
                'param' => ['field_empty' => ''],
                'required' => ['field_empty'],
            ],[
                'param' => ['field_null' => null],
                'required' => ['field_null'],
            ],[
                'param' => ['field_arr' => []],
                'required' => ['field_arr'],
            ],[
                'param' => ['field1' => 'a', 'field2' => 'b'],
                'required' => ['field1', 'field4'],
            ],
        ];
        foreach ($testFailSet as $set) {
            $result = Utility::checkRequired($set['param'], $set['required']);
            $this->assertNotEmpty($result['message']);
            $this->assertFalse($result['success'], 'Current set: ' . json_encode($set));
        }
    }

    public function testCreateQuerySort()
    {
        //test ASC case
        $testParam = ['sort' => 'title_asc'];
        $dibiSort = Utility::createQuerySort($testParam);
        $this->assertArrayHasKey('title', $dibiSort);
        $this->assertTrue($dibiSort['title']);

        //test DESC case
        $testParam = ['sort' => 'user_name_desc'];
        $dibiSort = Utility::createQuerySort($testParam);
        $this->assertArrayHasKey('user_name', $dibiSort);
        $this->assertFalse($dibiSort['user_name']);

        //test default case
        $testParam = [];
        $dibiSort = Utility::createQuerySort($testParam, 'id', 'desc');
        $this->assertArrayHasKey('id', $dibiSort);
        $this->assertFalse($dibiSort['id']);
    }

    public function testCreateQueryOffset()
    {
        $testSet = [
            ['page_num' => 1, 'page_limit' => 20, 'offset' => 0],
            ['page_num' => 5, 'page_limit' => 20, 'offset' => 80],
            ['page_num' => 10, 'page_limit' => 100, 'offset' => 900],
            ['page_num' => 99999999, 'page_limit' => 50, 'offset' => 4999999900], //exceed int(32)
            ['page_num' => 0, 'page_limit' => 0, 'offset' => 0],
        ];
        foreach ($testSet as $set) {
            $result = Utility::createQueryOffset($set['page_num'], $set['page_limit']);
            $this->assertEquals($set['offset'], $result, 'Current set: ' . json_encode($set));
        }
    }

    public function testCreateQueryOffset2()
    {
        $testSet = [
            ['page_num' => 1, 'page_limit' => 20, 'offset' => 0],
            ['page_num' => 5, 'page_limit' => 20, 'offset' => 80],
            ['page_num' => 10, 'page_limit' => 100, 'offset' => 900],
            ['page_num' => 99999999, 'page_limit' => 50, 'offset' => 4999999900], //exceed int(32)
            ['page_num' => 0, 'page_limit' => 0, 'offset' => 0],
        ];
        foreach ($testSet as $set) {
            $result = Utility::createQueryOffset2($set);
            $this->assertEquals($set['offset'], $result, 'Current set: ' . json_encode($set));
        }
    }

    public function testCreateUpdateSet()
    {
        $testSet = [
            [
                'param' => [],
                'col' => [],
                'expected' => [],
            ],[
                'param' => ['sno' => 1, 'title' => 'test', 'user' => 'BP MAN'],
                'col' => ['sno','title','user'],
                'expected' => ['sno' => 1, 'title' => 'test', 'user' => 'BP MAN'],
            ],[
                'param' => ['sno' => 1, 'title' => 'test', 'user' => 'BP MAN'],
                'col' => ['title','user'],
                'expected' => ['title' => 'test', 'user' => 'BP MAN'],
            ],[
                'param' => ['sno' => 1, 'title' => 'test', 'user' => 'BP MAN'],
                'col' => ['title', 'not_exist'],
                'expected' => ['title' => 'test'],
            ],
        ];
        foreach ($testSet as $set) {
            $result = Utility::CreateUpdateSet($set['param'], $set['col']);
            $this->assertEquals($set['expected'], $result, 'Current set: ' . json_encode($set));
        }
    }

    public function testToWesternDate()
    {
        $testSet = [
            ['ROC' => '1120131', 'AD' => '2023-01-31'],
            ['ROC' => '10101', 'AD' => '1912-01-01'],
            ['ROC' => '721010', 'AD' => '1983-10-10'],
            ['ROC' => '891111', 'AD' => '2000-11-11'],
            ['ROC' => '9991231', 'AD' => '2910-12-31'],
        ];
        foreach ($testSet as $set) {
            $adDate = Utility::toWesternDate($set['ROC']);
            $this->assertEquals($set['AD'], $adDate, 'Current set: ' . json_encode($set));
        }
    }

    public function testToROCDate()
    {
        $testSet = [
            ['ROC' => '1120131', 'AD' => '2023-01-31'],
            ['ROC' => '10101', 'AD' => '1912-01-01'],
            ['ROC' => '721010', 'AD' => '1983-10-10'],
            ['ROC' => '891111', 'AD' => '2000-11-11'],
            ['ROC' => '9991231', 'AD' => '2910-12-31'],
        ];
        foreach ($testSet as $set) {
            $rocDate = Utility::toROCDate($set['AD']);
            $this->assertEquals($set['ROC'], $rocDate, 'Current set: ' . json_encode($set));
        }
    }

    public function testGenerateRandomString()
    {
        $str = Utility::generateRandomString();
        $this->assertIsString($str);
        $this->assertEquals(10, strlen($str));

        $str = Utility::generateRandomString(18);
        $this->assertIsString($str);
        $this->assertEquals(18, strlen($str));

        $str = Utility::generateRandomString(5, 'dfg789');
        $this->assertIsString($str);
        $this->assertEquals(5, strlen($str));
        $this->assertMatchesRegularExpression('/^[d,f,g,7,8,9]{5}$/', $str);
    }

    public function testTimeAddColon()
    {
        $testSet = [
            '00:00' => '0000',
            '12:00' => '1200',
            '13:00' => 1300,
            '23:59' => '2359',
        ];
        foreach ($testSet as $expected => $testVal) {
            $this->assertEquals($expected, Utility::timeAddColon($testVal));
        }
    }

    public function testConvertFilePathArrayToString()
    {
        $testSet = [
            [
                'file_path' => '/tmp/test/aaa',
                'default_path' => '',
                'expected' => '/tmp/test/aaa',
            ],[
                'file_path' => '',
                'default_path' => '/tmp/test/bbb',
                'expected' => '/tmp/test/bbb',
            ],[
                'file_path' => ['tmp', 'test2', 'ccc'],
                'default_path' => '/tmp/test/aaa',
                'expected' => '/tmp/test2/ccc',
            ],[
                'file_path' => [],
                'default_path' => '/tmp/test/ddd',
                'expected' => '/tmp/test/ddd',
            ],[
                'file_path' => ['', 'tmp', 'test2', 'eee', 'ffff', 'ggggg'],
                'default_path' => '/tmp/test/aaa',
                'expected' => '//tmp/test2/eee/ffff/ggggg',
            ],
        ];
        foreach ($testSet as $set) {
            $result = Utility::convertFilePathArrayToString($set['file_path'], $set['default_path']);
            $this->assertEquals($set['expected'], $result, 'Current set: ' . json_encode($set));
        }
    }

    public function testGetNextDayBeginning()
    {
        $testSet = [
            '2023-02-04 00:00:00' => '2023-02-03',
            '2023-02-04 00:00:00' => '2023/2/3 23:59:59',
            '2024-01-01 00:00:00' => '2023-12-31 01:50:30',
            '2024-12-31 00:00:00' => '2024-12-30',
            '2024-02-29 00:00:00' => '2024-02-28',
            '2024-03-01 00:00:00' => '2024-02-29',
        ];

        foreach ($testSet as $expected => $testVal) {
            $this->assertEquals($expected, Utility::getNextDayBeginning($testVal));
        }
        $this->assertFalse(Utility::getNextDayBeginning('abcdefg'));
    }
}