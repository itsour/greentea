<?php
use App\Core\Config\Config;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . "/../../vendor/autoload.php";

class ConfigTest extends TestCase 
{
    public function testGetAppEnv()
    {
        $currAppEnv = Config::getAppEnv();
        //print_r($currAppEnv);
        $this->assertContains($currAppEnv, ['dev', 'testing', 'staging', 'production']);
    }

    public function testGetEnv()
    {
        $env = Config::_getEnv('database');
        //print_r($env);
        $this->assertArrayHasKey('main', $env);
    }

    public function testGetBpFrameworkVersion()
    {
        $frameworkVersion = Config::getBpFrameworkVersion();
        $this->assertNotEquals('Unknown', $frameworkVersion);
    }

    public function testGetAppEnvForApiResult()
    {
        $config = new Config();
        $result = $config->getAppEnvForApiResult();
        $this->assertArrayHasKey('payload', $result);
        $this->assertEquals(true, $result['success']);
        $this->assertArrayHasKey('env', $result['payload']);
    }

    public function testGetConfig()
    {
        $config = new Config();
        $result = $config('system.web.system_url');
        $this->assertNotEmpty($result);

        $defaultValue = 'Mark II';
        $result = $config('system.setting.not.exist', $defaultValue);
        $this->assertEquals($defaultValue, $result);
    }
}