<?php
use PHPUnit\Framework\TestCase;
use App\Core\Debug\Debug;

require_once __DIR__ . "/../../vendor/autoload.php";

class DebugTest extends TestCase 
{
    public function testLevel5Debug()
    {
        $debug = new Debug(5);
        $debug->beforeRoute();
        $debugInfo = $debug->afterRoute();
        $this->assertArrayHasKey('usageSeconds', $debugInfo);
        $this->assertGreaterThanOrEqual(0, $debugInfo['usageSeconds']);
    }
}