<?php
use App\Service\DemoService;
use App\Core\Common\ApiResultImpl;
use App\Service\DemoController;
use PHPUnit\Framework\TestCase;
use Slim\Factory\ServerRequestCreatorFactory;
use Slim\Psr7\Factory\ResponseFactory;

require_once __DIR__ . "/../../vendor/autoload.php";

/**
 * 透過DemoController來測試框架的BaseController
 */
class BaseControllerTest extends TestCase 
{
    private $serverReqCreator;
    private $respCreator;
    
    protected function setUp(): void
    {
        $this->serverReqCreator = ServerRequestCreatorFactory::create();
        $this->respCreator = new ResponseFactory();
    }

    // detail method
    public function testDetailCheckIdFail()
    {
        $request = $this->serverReqCreator->createServerRequestFromGlobals();
        $response = $this->respCreator->createResponse();

        $service = new DemoController();
        $responseResult = $service->detail($request, $response, []);
        // check status code
        $statusCode = $responseResult->getStatusCode();
        $this->assertEquals(200, $statusCode, 'Status code should be 200');
        // check body is valid json
        $body = $responseResult->getBody();
        $this->assertStringStartsWith('{', $body, 'Response body not json');
        $arrBody = json_decode($body, true);
        $this->assertIsArray($arrBody);
        // check response content
        $this->assertEquals(false, $arrBody['success'], 'success should be false');
        $this->assertEquals(ApiResultImpl::CODE_CLIENT_FAIL, $arrBody['code']);
    }

    // detail method
    public function testDetailDataNotFoundFail()
    {
        $request = $this->serverReqCreator->createServerRequestFromGlobals();
        $response = $this->respCreator->createResponse();

        $param = ['id' => 111];
        $request = $request->withQueryParams($param);
        $service = new DemoController();
        $responseResult = $service->detail($request, $response, []);

        // check body is valid json
        $body = $responseResult->getBody();
        $this->assertStringStartsWith('{', $body, 'Response body not json');
        $arrBody = json_decode($body, true);
        $this->assertIsArray($arrBody);
        // check response content
        $this->assertEquals(false, $arrBody['success'], 'success should be false');
        $this->assertEquals(ApiResultImpl::CODE_CLIENT_FAIL, $arrBody['code']);
    }

    // detail method
    public function testDetailSuccess()
    {
        $request = $this->serverReqCreator->createServerRequestFromGlobals();
        $response = $this->respCreator->createResponse();

        $param = ['id' => 4];
        $request = $request->withQueryParams($param);
        $service = new DemoController();
        $responseResult = $service->detail($request, $response, []);

        // check body is valid json
        $body = $responseResult->getBody();
        $this->assertStringStartsWith('{', $body, 'Response body not json');
        $arrBody = json_decode($body, true);
        $this->assertIsArray($arrBody);

        $this->assertEquals(true, $arrBody['success']);
        $this->assertEquals(ApiResultImpl::CODE_SUCCESS, $arrBody['code']);
        $this->assertArrayHasKey('payload', $arrBody);
        $this->assertArrayHasKey('id', $arrBody['payload']);
        $this->assertArrayHasKey('title', $arrBody['payload']);
    }

    public function testAddFail()
    {
        $request = $this->serverReqCreator->createServerRequestFromGlobals();
        $response = $this->respCreator->createResponse();

        $service = new DemoController();
        $responseResult = $service->add($request, $response, []);
        // check status code
        $statusCode = $responseResult->getStatusCode();
        $this->assertEquals(200, $statusCode, 'Status code should be 200');

        // check body is valid json
        $body = $responseResult->getBody();
        $this->assertStringStartsWith('{', $body, 'Response body not json');
        $arrBody = json_decode($body, true);
        $this->assertIsArray($arrBody);

        $this->assertEquals(false, $arrBody['success'], 'success should be false');
        $this->assertEquals(ApiResultImpl::CODE_SERVER_FAIL, $arrBody['code']);
    }
}