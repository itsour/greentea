<?php
use App\Core\Common\ApiResultImpl;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . "/../../vendor/autoload.php";

class ApiResultTest extends TestCase 
{
    public function testSuccessWithPayload()
    {
        $api = new ApiResultImpl();

        $testData = $this->getTestPayload();
        $msg = $this->getTestMessage();
        $apiResult = $api->setSuccess('not final payload');
        $apiResult = $api->setSuccess($testData, $msg);
        $this->assertEquals(true, $apiResult['success'], 'success should be true');
        $this->assertEquals(200, $apiResult['code']);
        $this->assertEquals($msg, $apiResult['s_message']);
        $this->assertEquals($testData, $apiResult['payload']);

        $testDebug = $this->getTestDebug();
        $apiResult = $api->setSuccess([], $msg, null, null, $testDebug);
        $this->assertEquals($testDebug, $apiResult['debug']);
        $this->assertEquals([], $apiResult['payload']);
    }

    public function testFail()
    {
        $api = new ApiResultImpl();

        $testData = $this->getTestPayload();
        $msg = $this->getTestMessage();
        $apiResult = $api->setClientFail($testData, $msg);
        $this->assertEquals(false, $apiResult['success'], 'success should be false');
        $this->assertEquals(ApiResultImpl::CODE_CLIENT_FAIL, $apiResult['code']);
        $this->assertEquals($msg, $apiResult['s_message'], 'message not the same');
        $this->assertEquals($testData, $apiResult['payload']);

        $testDebug = $this->getTestDebug();
        $reviseCode = 404;
        $apiResult = $api->setFail($reviseCode, null, $msg, null, null, $testDebug);
        $this->assertEquals($reviseCode, $apiResult['code']);
        $this->assertEquals($testDebug, $apiResult['debug']);
        $this->assertArrayNotHasKey('payload', $apiResult);
    }

    public function testSetAndUnsetExtra()
    {
        $api = new ApiResultImpl();
        $testArr = [
            'key_str' => 'Lord of The Ring',
            'key_arr' => ['You', 'Can', 'Not', 'Pass'],
            '1' => 1
        ];

        foreach ($testArr as $key => $value) {
            $api->unsetExtraApiResult($key);
            $this->assertArrayNotHasKey($key, $api->apiResult);
        }

        foreach ($testArr as $key => $value) {
            $api->setExtraApiResult($key, $value);
            $this->assertEquals($value, $api->apiResult[$key]);
        }

        foreach ($testArr as $key => $value) {
            $api->unsetExtraApiResult($key);
            $this->assertArrayNotHasKey($key, $api->apiResult);
        }
    }

    public function testSetExtraException1()
    {
        $testArr = ['success', 'code', 'message', 'payload', ''];
        $api = new ApiResultImpl();
        $this->expectException(InvalidArgumentException::class);
        $api->setExtraApiResult('success', '');
    }

    public function testSetExtraException2()
    {
        $api = new ApiResultImpl();
        $this->expectException(InvalidArgumentException::class);
        $api->setExtraApiResult('s_message', '');
    }

    public function testSetApiResult()
    {
        $api = new ApiResultImpl();
        $testData = $this->getTestPayload();
        $msg = $this->getTestMessage();
        $api->setApiResult(true, 9999, $testData, $msg);
        
        $this->assertEquals(9999, $api->apiResult['code']);
        $this->assertEquals($msg, $api->apiResult['s_message'], 'message not the same');
        $this->assertEquals($testData, $api->apiResult['payload']);
        
        //set again
        $api->setApiResult(false, 87, [], '', null, null, 'debug');
        $this->assertEquals(87, $api->apiResult['code']);
        $this->assertEquals('', $api->apiResult['s_message'], 'message should be empty string');
        $this->assertEquals([], $api->apiResult['payload']);
        $this->assertEquals(['debug'], $api->apiResult['debug']);
    }

    private function getTestPayload()
    {
        return array(
            'test_str' => 'test data',
            'test_arr' => array('a','b','c')
        );
    }

    private function getTestMessage()
    {
        return 'this is test message';
    }

    private function getTestDebug()
    {
        return [
            'debug_str' => 'this is debug string',
            'debug_arr' => ['d1', 'd2', 'd3']
        ];
    }
}