<?php
use PHPUnit\Framework\TestCase;
use App\Core\Logger\FileLogger;
use App\Core\Config\Config;

require_once __DIR__ . "/../../vendor/autoload.php";

class FileLoggerTest extends TestCase 
{
    public function testFileLog()
    {
        $config = new Config();
        $fileLogPath = $config('system.file_log.log_folder_path');
        $isRotate = $config('system.file_log.is_rotate', false);
        $logMsg = 'unit test for file log';
        $logName = 'bp_log.log';
        $realLogName = $this->getRealLogName($logName, $isRotate);
        FileLogger::info($logMsg, [], $logName);

        if (is_array($fileLogPath)) {
            $fileLogPath = DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $fileLogPath);
        }
        $fileLogPath .= DIRECTORY_SEPARATOR . $realLogName;

        $logs = file($fileLogPath);
        $logged = $logs[count($logs) - 1];

        $this->assertStringContainsString($logMsg, $logged);
    }

    private function getRealLogName(string $logName, bool $isRotate)
    {
        if ($isRotate) {
            $arrLogName = explode('.', $logName);
            $extension = array_pop($arrLogName);
            return implode('.', $arrLogName) . '-' . date('Y-m-d') . '.' . $extension;
        } else {
            return $logName;
        }
    }
}