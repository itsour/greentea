<?php
use App\Module\FileServer\FileServer;
use App\Core\Config\Config;
use App\Utility\Utility;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . "/../../vendor/autoload.php";

class FileServerTest extends TestCase 
{
    public function testConnectAndWriteAndDeleteFile()
    {
        $fsPath = FileServer::connect('file_server');
        $config = new Config;
        $configFSPath = Utility::convertFilePathArrayToString($config('system.file_server.file_system_root_path'));
        $this->assertNotFalse($fsPath, 'Get file server path fail');
        $this->assertEquals($configFSPath, $fsPath);

        if (false != $fsPath) {
            $fileFullPath = $fsPath . DIRECTORY_SEPARATOR .'test_fs.txt';
            touch($fileFullPath);
            $this->assertFileExists($fileFullPath);
            unlink($fileFullPath);
            $this->assertFileDoesNotExist($fileFullPath);
        }
    }
}