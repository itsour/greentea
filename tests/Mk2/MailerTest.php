<?php
use App\Core\Mailer\Mailer;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . "/../../vendor/autoload.php";

class MailerTest extends TestCase 
{
    public function testMailerInstance()
    {
        $mailer = Mailer::getInstance('main');
        $this->assertIsObject($mailer);
    }

    public function testSendMail()
    {
        //目前使用的是PHPMailer物件
        $mailer = Mailer::getInstance('main');
        $this->assertIsObject($mailer);
        
        //由於Google限制嚴格,故不測真正的寄信行為
        $mailFrom = 'choutest19@gmail.com';
        $mailer->setFrom($mailFrom);
        $mailer->addAddress('martinhsu@blueplanet.com.tw');
        $mailer->Subject = 'BP Mark2 Mail Test';
        $mailBody = 'This is a test mail. Do not reply.';
        $mailer->Body = $mailBody;

        // $this->assertObjectHasAttribute('From', $mailer);
        // $this->assertObjectHasAttribute('Subject', $mailer);
        $this->assertEquals($mailBody, $mailer->Body);
        $this->assertEquals($mailFrom, $mailer->From);
    }
}