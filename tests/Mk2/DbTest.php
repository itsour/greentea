<?php
use PHPUnit\Framework\TestCase;
use App\Core\Db\Db;

require_once __DIR__ . "/../../vendor/autoload.php";

/**
 * 測試Db的資料庫存取
 */
class DbTest extends TestCase 
{
    public function testMultiVenderDbConnect()
    {
        $db1 = Db::getInstance('main');
        $db2 = Db::getInstance('tccnk');

        $this->assertInstanceOf(Dibi\Connection::class, $db1);
        $this->assertInstanceOf(Dibi\Connection::class, $db2);


        $result1 = $db1->fetchAll("SELECT TOP(1) * FROM person");
        $result2 = $db2->fetchAll("SELECT * FROM `news` LIMIT 1");

        $this->assertNotEmpty($result1);
        $this->assertNotEmpty($result2);
    }
}