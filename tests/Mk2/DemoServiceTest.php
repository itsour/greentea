<?php
use App\Service\DemoService;
use App\Core\Common\ApiResultImpl;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . "/../../vendor/autoload.php";

class DemoServiceTest extends TestCase 
{
    // detail method
    public function testDetailCheckIdFail()
    {
        $param = array();
        $service = new DemoService;
        $apiResult = $service->detail($param);
        $this->assertEquals(false, $apiResult['success'], 'success should be false');
        $this->assertEquals(ApiResultImpl::CODE_CLIENT_FAIL, $apiResult['code']);
    }

    // detail method
    public function testDetailDataNotFoundFail()
    {
        $param = array('id'=>111);
        $service = new DemoService;
        $apiResult = $service->detail($param);
        $this->assertEquals(false, $apiResult['success'], 'success should be false');
        $this->assertEquals(ApiResultImpl::CODE_CLIENT_FAIL, $apiResult['code']);
    }

    // detail method
    public function testDetailSuccess()
    {
        $param = array('id'=>4);
        $service = new DemoService;
        $apiResult = $service->detail($param);
        $this->assertEquals(true, $apiResult['success']);
        $this->assertEquals(ApiResultImpl::CODE_SUCCESS, $apiResult['code']);
        $this->assertArrayHasKey('payload', $apiResult);
        $this->assertArrayHasKey('id', $apiResult['payload']);
        $this->assertArrayHasKey('title', $apiResult['payload']);
    }

    public function testAddFail()
    {
        $service = new DemoService;
        $apiResult = $service->add([], []);
        $this->assertEquals(false, $apiResult['success'], 'success should be false');
        $this->assertEquals(ApiResultImpl::CODE_SERVER_FAIL, $apiResult['code']);
    }

    public function testCustomFail()
    {
        $testCode = 404;
        $service = new DemoService;
        $apiResult = $service->clientFail($testCode);
        $this->assertEquals(false, $apiResult['success'], 'success should be false');
        $this->assertEquals($testCode, $apiResult['code']);

        $testCode = 505;
        $service = new DemoService;
        $apiResult = $service->serverFail($testCode);
        $this->assertEquals(false, $apiResult['success'], 'success should be false');
        $this->assertEquals($testCode, $apiResult['code']);
    }
}