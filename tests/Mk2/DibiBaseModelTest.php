<?php
use App\Core\Common\DibiBaseModel;
use PHPUnit\Framework\TestCase;
use App\Core\Common\ApiResultImpl;

require_once __DIR__ . "/../../vendor/autoload.php";

/**
 * 測試DibiBaseModel的資料庫存取
 * **假設已有一張mk2_test的資料表
 */
class DibiBaseModelTest extends TestCase 
{
    public function testInsertGetDeleteData()
    {
        $dbmObj = new DibiBaseModel();
        $dbmObj->setTable('mk2_test');
        $utf8Str = '王綉春在「瀞水」邊聽著，羣（群）牛犇（奔）來之聲。';
        $testData = array(
            'short_char' => 'This is blueplanet framework mark2 test',
            'utf8_char' => $utf8Str,
            'long_char' => '',
            'input_date' => date('Y-m-d H:i:s')
        );
        //test insert and select data
        $sno = $dbmObj->insertData($testData);
        $data = $dbmObj->getDataByKey('sno', $sno);
        $this->assertArrayHasKey('sno', $data);
        $this->assertEquals($sno, $data['sno']);
        $this->assertEquals($utf8Str, $data['utf8_char']);
        print_r(PHP_EOL . "current sno:" . $sno . PHP_EOL);

        //test update data
        $updateData = array(
            'long_char' => $utf8Str,
        );
        $dbmObj->updateDataByKey($updateData, 'sno', $sno);
        $data = $dbmObj->getDataByKey('sno', $sno);
        $this->assertArrayHasKey('long_char', $data);
        $this->assertEquals($utf8Str, $data['long_char']);

        //test delete data
        $dbmObj->deleteDataByKey('sno', $sno);
        $data = $dbmObj->getDataByKey('sno', $sno);
        $this->assertEmpty($data);
    }

    /**
     * 測試當給錯vendor設定,DibiBaseModel有正確拋出exception
     */
    public function testConnectException()
    {
        $this->expectException(RuntimeException::class);
        $dbmObj = new DibiBaseModel('test_for_not_exist_setting');
    }

    public function testDbOperateWithoutLog()
    {
        $dbmObj = new DibiBaseModel('main', false);
        $dbmObj->setTable('mk2_test');
        $utf8Str = '只是測試';
        $testData = array(
            'short_char' => 'This is blueplanet framework mark2 test',
            'utf8_char' => $utf8Str,
            'long_char' => '',
            'input_date' => date('Y-m-d H:i:s')
        );
        //test insert and select data
        $sno = $dbmObj->insertData($testData);
        $data = $dbmObj->getDataByKey('sno', $sno);
        $this->assertArrayHasKey('long_char', $data);
        $this->assertEquals($sno, $data['sno']);
        $this->assertEquals($utf8Str, $data['utf8_char']);

        //test delete data
        $dbmObj->deleteDataByKey('sno', $sno);
        $data = $dbmObj->getDataByKey('sno', $sno);
        $this->assertEmpty($data);
    }

    public function testSuccessWithDebug()
    {
        $dbm = new DibiBaseModel('main', true);

        $testData = $this->getTestPayload();
        $msg = $this->getTestMessage();
        $dbm->setSuccess('not final payload');
        $dbm->setSuccess($testData, $msg);
        $apiResult = $dbm->getApiResult();
        $this->assertEquals(true, $apiResult['success']);
        $this->assertEquals(200, $apiResult['code']);
        $this->assertEquals($msg, $apiResult['s_message']);
        $this->assertEquals($testData, $apiResult['payload']);

        $testDebug = $this->getTestDebug();
        $dbm->setSuccess($testData, $msg, null, null, $testDebug);
        $apiResult = $dbm->getApiResult();
        $this->assertEquals(true, $apiResult['success']);
        $this->assertEquals($testDebug, $apiResult['debug']);
    }


    public function testFailWithDebug()
    {
        $dbm = new DibiBaseModel();

        $testData = $this->getTestPayload();
        $msg = $this->getTestMessage();
        $dbm->setFail(ApiResultImpl::CODE_SERVER_FAIL, 'not final payload');
        $dbm->setFail(ApiResultImpl::CODE_SERVER_FAIL, $testData, $msg);
        $apiResult = $dbm->getApiResult();
        $this->assertEquals(false, $apiResult['success']);
        $this->assertEquals(ApiResultImpl::CODE_SERVER_FAIL, $apiResult['code']);
        $this->assertEquals($msg, $apiResult['s_message']);

        $testDebug = $this->getTestDebug();
        $reviseCode = 499;
        $dbm->setFail($reviseCode, null, $msg, null, null, $testDebug);
        $apiResult = $dbm->getApiResult();
        $this->assertEquals(false, $apiResult['success']);
        $this->assertEquals($reviseCode, $apiResult['code']);
        $this->assertEquals($testDebug, $apiResult['debug']);
        $this->assertArrayNotHasKey('payload', $apiResult);
    }

    //此測試太花時間,暫不使用
    // public function testCleanupLogSystem()
    // {
    //     $dbmObj = new DibiBaseModel();
    //     $dbmObj->cleanupLogSystem();
    //     $config = new Config();
    //     $keepDays = $config('system_log.db_log_keep_days');
    //     $intervalStr = 'P' . $keepDays . 'D';
    //     $date = new \DateTime();
    //     $date->sub(new \DateInterval($intervalStr));
    //     $date_string = $date->format('Y-m-d H:i:s.u');
    //     $countLogs = $this->db->query("SELECT COUNT(1) FROM `log_system` WHERE created_time < '".$date_string."'");
    //     $this->assertEquals(0, $countLogs);
    // }

    private function getTestPayload()
    {
        return array(
            'test_str' => 'test data',
            'test_arr' => array('a','b','c')
        );
    }

    private function getTestMessage()
    {
        return 'this is test message';
    }

    private function getTestDebug()
    {
        return [
            'debug_str' => 'this is debug string',
            'debug_arr' => ['d1', 'd2', 'd3']
        ];
    }

    public function testInsertGetDeleteDataByMultiKey()
    {
        $dbmObj = new DibiBaseModel();
        $dbmObj->setTable('mk2_test');
        $utf8Str = 'test data: 王綉春在「瀞水」邊聽著，羣（群）牛犇（奔）來之聲';
        $shortChar = 'This is blueplanet framework mark2 test' . date('YmdHis');
        $longChar = 'not so long';
        $inputDate = date('Y-m-d H:i:s');
        $testData = [
            'short_char' => $shortChar,
            'utf8_char' => $utf8Str,
            'long_char' => $longChar,
            'input_date' => $inputDate
        ];
        //test insert and select data
        $sno = $dbmObj->insertData($testData);

        //insert second row
        $testData2 = $testData;
        $testData2['utf8_char'] = 'This is second row';
        $sno2 = $dbmObj->insertData($testData2);

        //test get by multi keys
        $multiWhereKeys = [
            'utf8_char' => $utf8Str,
            'short_char' => $shortChar,
        ];
        $data = $dbmObj->getDataByMultiKeys($multiWhereKeys);
        $this->assertNotEmpty($data);
        $this->assertEquals($shortChar, $data['short_char']);
        $this->assertEquals($utf8Str, $data['utf8_char']);

        $multiWhereKeys2 = [
            'sno' => $sno
        ];
        $data = $dbmObj->getDataByMultiKeys($multiWhereKeys2);
        $this->assertNotEmpty($data);
        $this->assertEquals($shortChar, $data['short_char']);

        //test update data
        $longChar = 'not soooooooooooooooooooooooooooooooooooooooooooooooooooooooo long';
        $updateData = [
            'long_char' => $longChar
        ];
        $dbmObj->updateDataByMultiKeys($updateData, $multiWhereKeys);
        $data = $dbmObj->getDataByKey('sno', $sno);
        $this->assertEquals($longChar, $data['long_char']);

        //test get multiple rows by single key-value
        $multiRows = $dbmObj->getRowsByKey('short_char', $shortChar);
        $this->assertIsArray($multiRows);
        $this->assertCount(2, $multiRows);

        //test get multiple rows by multi-key pairs
        $multiWhereKeys3 = [
            'short_char' => $shortChar,
            'input_date' => $inputDate
        ];
        $multiRows2 = $dbmObj->getRowsByMultiKeys($multiWhereKeys3);
        $this->assertIsArray($multiRows2);
        $this->assertCount(2, $multiRows2);

        //test delete data
        $dbmObj->deleteDataByMultiKeys($multiWhereKeys);
        $data = $dbmObj->getDataByKey('sno', $sno);
        $this->assertEmpty($data);
    }

    public function testDbOperateMethods()
    {
        $dbmObj = new DibiBaseModel('main', false);
        $dbmObj->setTable('mk2_test');
        $utf8Str = '測試輸入的內容';
        $testData = array(
            'short_char' => 'Blueplanet framework Mark2 test',
            'utf8_char' => $utf8Str,
            'long_char' => 'Looooooooooooooooooooooooooooooooooong string',
            'input_date' => date('Y-m-d')
        );

        // test query method
        $sno = $dbmObj->insertData($testData);
        $qr = $dbmObj->query('SELECT COUNT(1) FROM mk2_test');
        $count = $qr->fetchSingle();
        $this->assertGreaterThan(0, $count);

        // test fetch method
        $row = $dbmObj->fetch('SELECT * FROM mk2_test WHERE sno = %i', $sno);
        $this->assertNotEmpty($row);
        $this->assertEquals($testData['utf8_char'], $row['utf8_char']);

        // test fetchAll method
        $rows = $dbmObj->fetchAll('SELECT * FROM mk2_test WHERE input_date >= %s', date('Y-m-d', strtotime('-1 day')));
        $this->assertNotEmpty($rows);

        // test fetchSingle method
        $result = $dbmObj->fetchSingle('SELECT long_char, utf8_char FROM mk2_test WHERE sno = %i', $sno);
        $this->assertEquals($testData['long_char'], $result);

        // test fetchAssoc
        $rows = $dbmObj->fetchAssoc('sno', 'SELECT * FROM mk2_test WHERE input_date >= %s', date('Y-m-d', strtotime('-1 day')));
        $this->assertNotEmpty($rows);

        // test fetchPair
        $rows = $dbmObj->fetchPairs('SELECT sno, short_char FROM mk2_test WHERE input_date >= %s', date('Y-m-d', strtotime('-1 day')));
        $this->assertNotEmpty($rows);

        // test transaction begin and commit
        $testUpdateData = array(
            'short_char' => 'Blueplanet framework Mark2 test modify',
            'long_char' => 'Very Loooooooooooooooooooooooooooooooooooooooooooooooooooong String',
        );
        $dbmObj->begin();
        $dbmObj->query('UPDATE mk2_test SET ', $testUpdateData, ' WHERE sno = %i', $sno);
        $dbmObj->commit();
        $result = $dbmObj->fetch('SELECT * FROM mk2_test WHERE sno = %i', $sno);
        $this->assertEquals($testUpdateData['short_char'], $result['short_char']);
        $this->assertEquals($testUpdateData['long_char'], $result['long_char']);

        //test delete data
        $dbmObj->deleteDataByKey('sno', $sno);
        $data = $dbmObj->getDataByKey('sno', $sno);
        $this->assertEmpty($data);
    }
}