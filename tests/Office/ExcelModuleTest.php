<?php

namespace tests\Office;

use App\Module\Office\ExcelModuleImpl;
use App\Module\Office\OfficeFactory;
use PHPUnit\Framework\TestCase;

class ExcelModuleTest extends TestCase
{
    public function testOfficeFactory()
    {
        $type = 'xlsx';
        $module = OfficeFactory::createOfficeModule($type, 'testInit');
        $this->assertEquals(true, is_a($module, 'App\Module\Office\ExcelModuleImpl'));
    }

    public function testOfficeFactoryNonSupportFormat()
    {
        $type = 'test';
        $this->expectExceptionMessage($type . " 不在工廠方法支援清單內。");
        OfficeFactory::createOfficeModule($type, 'testInit');
    }

    public function testInitExtension()
    {
        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);

        $this->assertEquals(strtolower($testCaseExtension), $module->getExtension());
    }

    public function testInitFilename()
    {
        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);

        $this->assertEquals(
            $testCaseFilename . '.' . strtolower($testCaseExtension),
            $module->getFileName()
        );
    }

    public function testInitEmptyFilename()
    {
        $testCaseExtension = 'Xlsx';
        $module = new ExcelModuleImpl($testCaseExtension, '');

        $this->assertEquals(
            time() . '.' . strtolower($testCaseExtension),
            $module->getFileName()
        );
    }

    public function testInitNonSuppotFormat()
    {
        $this->expectExceptionMessage("本模組只支援 xlsx(After 2007), xlsb(After 2007 Binrary), xls(Behind 2007)");

        $testCaseExtension = 'pdf';
        $testCaseFilename = 'testInit';

        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);
    }

    public function testMAXSheetCreate()
    {
        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $testSheetCount = 10;

        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);
        $module->setMAXCountSheet($testSheetCount);
        $this->assertEquals($testSheetCount, $module->getSheetCount());
    }

    public function testSetContent()
    {
        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);

        $testContent = $this->getTestContent();
        $module->setContent($testContent);

        $this->assertEquals($testContent, $module->getContent());
    }

    public function testSetContentNonArray()
    {
        $this->expectExceptionMessage('excel content 需採用三維陣列。針對 "分頁" -> "列" -> "行"');

        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);

        $testContent = [];
        $module->setContent($testContent);
    }

    public function testSetSheetIndex()
    {
        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $testSheetIndex = 5;

        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);
        $module->setCurrentSheetIndex($testSheetIndex);
        $this->assertEquals(($testSheetIndex + 1), $module->getSheetCount());
    }

    public function testSetPageContent()
    {
        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $testContent = [];
        $testCurrentIndex = 0;

        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);
        $module->setCurrentSheetIndex($testCurrentIndex);
        $module->setPageContent($testContent);

        $this->assertEquals([$testCurrentIndex => $testContent], $module->getContent());
    }

    public function testSetPageContentNonSetCurrentIndex()
    {
        $this->expectExceptionMessage('Sheet Index not Set. You Must Call setCurrentSheetIndex');

        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);

        $testContent = [];
        $module->setPageContent($testContent);
    }

    public function testSetPagename()
    {
        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);

        $testContent = $this->getTestContent();
        $module->setContent($testContent);

        $testPageTitle = $this->getTestPageName();
        $module->setPageName($testPageTitle[0]);

        $this->assertEquals($testPageTitle[0], $module->getPageName());
    }

    public function testSaveToFile()
    {
        $testCaseExtension = 'Xlsx';
        $testCaseFilename = 'testInit';
        $module = new ExcelModuleImpl($testCaseExtension, $testCaseFilename);

        $testContent = $this->getTestContent();
        $module->setContent($testContent);

        $testPath = sys_get_temp_dir();
        $module->saveToFile($testPath);

        $this->assertEquals(true, \is_file($testPath . DIRECTORY_SEPARATOR . $module->getFileName()));
    }

    private function getTestContent()
    {
        return [
            0 => [
                ['a', 'b', 'c', 'd'],
                ['e', 'f', 'g', 'h'],
                ['i', 'j', 'k', 'l']
            ],
            1 => [
                ['m', 'n', 'o', 'p'],
                ['q', 'r', 's', 't'],
                ['u', 'v', 'w', 'x'],
                ['y', 'z']
            ]
        ];
    }

    private function getTestPageName()
    {
        return [
            0 => '第一頁',
            1 => '第二頁'
        ];
    }
}
