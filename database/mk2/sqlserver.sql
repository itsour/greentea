/****** Object:  Table [dbo].[log_system]    Script Date: 2018/7/31 上午 09:41:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[log_system](
	[sno] [bigint] IDENTITY(1,1) NOT NULL,
	[created_time] [datetime2](7) NOT NULL,
	[acc_ip] [varchar](46) NULL,
	[acc_url] [nvarchar](1000) NULL,
	[acc_class] [varchar](255) NULL,
	[acc_method] [varchar](255) NULL,
	[class_usage_seconds] [float] NULL,
	[session] [nvarchar](max) NULL,
	[request] [nvarchar](max) NULL,
	[result] [nvarchar](max) NULL,
	[acc_from] [nvarchar](1000) NULL,
	[agent] [nvarchar](max) NULL
 CONSTRAINT [PK_log_system] PRIMARY KEY CLUSTERED 
(
	[sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mk2_test]    Script Date: 2018/7/31 上午 09:41:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mk2_test](
	[sno] [bigint] IDENTITY(1,1) NOT NULL,
	[short_char] [varchar](255) NULL,
	[utf8_char] [nvarchar](1000) NULL,
	[long_char] [nvarchar](max) NULL,
	[input_date] [date] NULL,
	[created_time] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[log_system] ADD  CONSTRAINT [DF_log_system_created_time]  DEFAULT (getdate()) FOR [created_time]
GO
ALTER TABLE [dbo].[mk2_test] ADD  CONSTRAINT [DF_mk2_test_created_time]  DEFAULT (getdate()) FOR [created_time]
GO