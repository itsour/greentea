CREATE TABLE IF NOT EXISTS `log_system` (
  `sno` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '創建時間',
  `acc_ip` varchar(46) CHARACTER SET utf8 DEFAULT NULL COMMENT '連入IP',
  `acc_url` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT '存取的url',
  `acc_class` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '存取的class名稱',
  `acc_method` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '存取的method名稱',
  `class_usage_seconds` DOUBLE UNSIGNED NULL DEFAULT NULL COMMENT '類別的使用時間(=解構時間-建構時間),精度到微秒',
  `session` mediumtext CHARACTER SET utf8 COMMENT 'session內容',
  `request` mediumtext CHARACTER SET utf8 COMMENT '傳入的內容/參數',
  `result` mediumtext CHARACTER SET utf8 COMMENT '回傳的內容(apiResult)',
  `acc_from` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT '從哪裡連過來的',
  `agent` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '連入的agent資訊',
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='框架底層的系統log';


CREATE TABLE IF NOT EXISTS `mk2_test` (
  `sno` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `short_char` varchar(255) CHARACTER SET utf8 NOT NULL,
  `utf8_char` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `long_char` mediumtext CHARACTER SET utf8 NOT NULL,
  `input_date` date NOT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='框架單元測試用的table，不需放到產品環境上';