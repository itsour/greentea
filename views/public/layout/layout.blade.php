<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BP MarkII</title>
    <link rel="icon" href="./image/favicon.ico">
    <link rel="stylesheet" href="./css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous"></link>
    <link href="./css/aos.css" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
        <!-- HEADER -->
        <div class="header">
            <a class="logo" href="https://www.blueplanet.com.tw/" target="_blank" rel="noopener noreferrer"><img src="./image/bp-logo.svg" alt=""></a>
        </div>

        <!-- LANDING PAGE  MAIN -->
        <div class="landing">
            <div class="landingText" data-aos="fade-up" data-aos-duration="1000">
                <h1>Blue Planet<br>Mark II v4</h1>
                <h3>Special Thanks：藍星球的所有夥伴</h3>
                <div class="btn">
                    <a href="https://gitlab.blueplanet.com.tw/martinhsu/bp_framework_mark2" target="_blank" rel="noopener noreferrer"><em class="fab fa-gitlab"></em>Go To Gitlab</a>
                </div>
                <div class="btn">
                    <a href="https://gitlab.blueplanet.com.tw/martinhsu/bp_framework_mark2/-/releases" target="_blank" rel="noopener noreferrer"><em class="fas fa-edit"></em>Release Note</a>
                </div>
            </div>
            <div class="landingImage" data-aos="fade-down" data-aos-duration="2000">
                <img src="image/bg.svg" alt="">
            </div>
        </div>

        <!-- FOOTER -->
        <div class="footer">
            <p>Copyright © 2021 Blue Planet Inc. All rights reserved.</p>
        </div>
    </div>

    <script src="./js/aos.js"></script>
    <script nonce="nHEyhxlaUGSQRDBmOZJ3">
            AOS.init();
    </script>

</body>
</html>