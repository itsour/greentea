<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Service\DemoController;
use Slim\Routing\RouteCollectorProxy;

require_once __DIR__ . '/../../vendor/autoload.php';

$app->group('/api/demo-controller', function (RouteCollectorProxy $app) {
    $app->post('/add', [DemoController::class, 'add']);
    $app->post('/list', [DemoController::class, 'list']);
    $app->get('/detail', [DemoController::class, 'detail']);
    //$app->get('/try-error', [DemoController::class, 'tryError']);
});