<?php
/**
 * !!! 此檔案為範例檔,應於上線前移除 !!!
 */
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Service\DemoBenchmarkService;
use App\Utility\HttpUtility;

require_once __DIR__ . '/../../vendor/autoload.php';


$app->get('/benchmark/route', function(Request $request, Response $response, array $args) {
    $service = new DemoBenchmarkService();
    $result = $service->benchmarkRoute([]);
    return HttpUtility::responseWithJson($response, $result);
});