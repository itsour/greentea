<?php
use App\Utility\Utility;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Service\DemoServiceJwt;
use App\Utility\HttpUtility;
use Slim\Routing\RouteCollectorProxy;


require_once __DIR__ . '/../../vendor/autoload.php';

$app->group('/api/jwt/demo', function(RouteCollectorProxy $app) {
    $app->get('/testWithoutLogin', function(Request $request, Response $response, array $args) {
        $service = new DemoServiceJwt();
        $result = $service->testWithoutLogin();
        return HttpUtility::responseWithJson($response, $result);
    });

    $app->get('/testWithLogin', function(Request $request, Response $response, array $args) {
        $service = new DemoServiceJwt();
        $result = $service->testWithLogin();
        return HttpUtility::responseWithJson($response, $result);
    });

    $app->post('/login', function(Request $request, Response $response, array $args) {
        $param = $request->getParsedBody();
        $service = new DemoServiceJwt();
        $result = $service->login($param);
        return HttpUtility::responseWithJson($response, $result);
    });

    $app->post('/logout', function(Request $request, Response $response, array $args) {
        $param = $request->getParsedBody();
        $service = new DemoServiceJwt();
        $result = $service->logout();
        return HttpUtility::responseWithJson($response, $result);
    });
});