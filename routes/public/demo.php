<?php
/**
 * !!! 此檔案為範例檔,應於上線前移除 !!!
 */
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Model\ExampleSqlServer;
use App\Model\ExampleMySql;
use App\Model\Example;
use App\Service\DemoService;
use App\Utility\HttpUtility;
use Slim\Routing\RouteCollectorProxy;

require_once __DIR__ . '/../../vendor/autoload.php';

$app->group('/api/front/demo', function (RouteCollectorProxy $app) {
    $app->post('/add', function(Request $request, Response $response, array $args) {
        $param = $request->getParsedBody();
        $files = $request->getUploadedFiles();
        $service = new DemoService();
        $result = $service->add($param, $files);
        return HttpUtility::responseWithJson($response, $result);
    });

    $app->post('/list', function(Request $request, Response $response, array $args) {
        $param = $request->getParsedBody();
        $service = new DemoService();
        $result = $service->list($param);
        return HttpUtility::responseWithJson($response, $result);
    });

    $app->get('/detail', function(Request $request, Response $response, array $args) {
        $param = $request->getQueryParams();
        $service = new DemoService();
        $result = $service->detail($param);
        return HttpUtility::responseWithJson($response, $result);
    });
});

$app->group('/public/api/test', function (RouteCollectorProxy $app) {
    $app->get('/get-config', function(Request $request, Response $response, array $args) {
        //$param = $request->getQueryParams();
        $ex = new Example();
        $result = $ex->getConfig();
        return HttpUtility::responseWithJson($response, $result);
    });
    
    $app->get('/connect-db', function(Request $request, Response $response, array $args) {
        //$param = $request->getQueryParams();
        $ex = new Example();
        $result = $ex->connectDB();
        return HttpUtility::responseWithJson($response, $result);
    });
    
    $app->get('/download-excel', function(Request $request, Response $response, array $args) {
        $param = $request->getQueryParams();
        $ex = new Example();
        $ex->downloadExcel();
    
        $response = $response->withHeader('Content-Description', 'File Transfer')
        ->withHeader('Content-Type', 'application/octet-stream')
        ->withHeader('Content-Disposition', 'attachment;filename="test.xlsx"')
        ->withHeader('Expires', '0')
        ->withHeader('Cache-Control', 'must-revalidate')
        ->withHeader('Pragma', 'public');
        return $response;
    });
    
    $app->post('/debug-log', function(Request $request, Response $response, array $args) {
        $param = $request->getParsedBody();
        $ex = new Example();
        $result = $ex->debugLog($param);
        return HttpUtility::responseWithJson($response, $result);
    });
});
