<?php
use App\Core\Common\DibiBaseModel;

require_once __DIR__ . '/../vendor/autoload.php';

$dbm = new DibiBaseModel();
$dbm->cleanupLogSystem();