框架使用說明
===============
# 準備
1. 請先安裝好XAMPP 或 手動裝好Apache Web Server、MariaDB 10.1以上(含)、PHP 7.1
2. 然後裝Composer [下載連結](https://getcomposer.org/download/)
3. 安裝Git並在Windows上連接GitLab
- 下載Git後，在windows上透過SSH Key 連接GitLab，請參考如下文件: [在Windows下產生SSH key,並透過SSH key連結GitLab](https://paper.dropbox.com/doc/Git-WindowsSSH-keySSH-keyGitLab-MP8X9B1vEhvhwsMDbymcK)

# 如何從框架建立新專案

1. 先在GitLab上開好新專案的repository, 命名改成該專案的名稱, eg. bp_bsmi_km

2. 取得框架程式庫的bare clone
```
git clone --bare git@gitlab.blueplanet.com.tw:martinhsu/bp_framework_mark2.git
```

3. 用mirror push將複製下來的程式庫推到新的
```
cd bp_framework_mark2.git
git push --mirror git@gitlab.blueplanet.com.tw:martinhsu/bp_bsmi_km.git
```

4. 移除第2步的程式庫
```
cd ..
rm -rf bp_framework_mark2.git
```

5. 再將新專案clone下來
```
git clone git@gitlab.blueplanet.com.tw:{martinhsu/bp_bsmi_km.git}
```

[參考來源](https://help.github.com/articles/duplicating-a-repository/#platform-windows)

# 開始使用

## 1. git clone
(1) 下載Project方式: git clone Project位置。請先與系統管理員聯絡加入gitlab開發團隊
```
git clone {git_path}
```

(2) git clone完後第一件事: 到Project資料夾下，`以terminal 執行 composer install`

## 2. 設定Apache virtual host
(1) 以XAMPP為例，virtual host的設定位於 C:\xampp\apache\conf\extra\httpd-vhosts.conf

`假設專案中的public資料夾路徑為 c:\xampp\htdocs\project\public`
```
<VirtualHost *:80>
        ##ServerAdmin webmaster@project.dev
        DocumentRoot "C:/xampp/htdocs/project/public/"
        ServerName project.local
        ErrorLog "logs/project.local-error.log"
        CustomLog "logs/project.local-access.log" common
</VirtualHost>
```
- 注意: Domain 取名為 xxx.dev 將會造成NET::ERR_CERT_AUTHORITY_INVALID的錯誤訊息(HSTS)，可使用xxx.local或不使用.dev取名方式，來避免此問題。

## 3. 置入.env檔
(1) 將.env.example複製一份並直接在專案根目錄貼上後，更名為.env
(2) 依環境設定.env中的環境變數，例如:
```
APP_ENV=dev
```

## 4. 在連線資料庫中，建置框架所需資料表
- 若是SQL Server，將database/mk2/sqlserver.sql檔貼到Management Studio中執行