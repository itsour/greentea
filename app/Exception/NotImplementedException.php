<?php

namespace App\Exception;

use App\Core\Common\BaseException;

/**
 * Function not implement exception
 */
class NotImplementedException extends BaseException
{
    public function __construct($errorMessage, $showMessage=null, $formatMessage=null, $payload=null, $code=500) {
        parent::__construct($errorMessage, $showMessage, $formatMessage, $payload, $code);
    }
}