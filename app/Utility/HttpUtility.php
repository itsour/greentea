<?php
/**
 * 與Http, Route相關的工具程式
 */
namespace App\Utility;

use Psr\Http\Message\ResponseInterface as Response;

class HttpUtility
{
    /**
     * Download the $filePath
     *
     * @param Response $response
     * @param string $filePath 檔案位置
     * @param string $basename (optional)下載檔案名稱
     * @return void
     */
    public static function getDownloadResponse(Response $response, string $filePath, $basename = '')
    {
        if ($basename == null || $basename == '') {
            $basename = pathinfo($filePath, PATHINFO_BASENAME);
        }

        if (file_exists($filePath)) {
            $response = $response->withHeader('Content-Disposition', 'attachment;filename=' . rawurlencode($basename) . ";filename*=utf-8''" . rawurlencode($basename))
                ->withHeader('Content-Type', 'application/octet-stream')
                ->withHeader('Content-Description', 'File Transfer')
                ->withHeader('Expires', '0')
                ->withHeader('Cache-Control', 'must-revalidate')
                ->withHeader('Pragma', 'public')
                ->withHeader('Content-Length', filesize($filePath));
            readfile($filePath);
            return $response;
        } else {
            $result = array(
                'success' => false,
                'code' => 500,
                'message' => "File not exist"
            );
            return self::responseWithJson($response, $result);
        }
    }

    /**
     * 轉換為HTTP JSON回傳格式的工具程式
     *
     * @param ResponseInterface $response Response物件
     * @param mixed $result 欲回傳的結果,盡量以陣列為主
     * @param int $statusCode Response HTTP status code, default is 200
     * @param int $jsonFlags json encode Bitmask
     * @return ResponseInterface Response物件
     */
    public static function responseWithJson(Response $response, $result, int $statusCode=200, int $jsonFlags=JSON_UNESCAPED_UNICODE): Response
    {
        $payload = json_encode($result, $jsonFlags);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}