<?php
/**
 * 框架Mark2工具程式
 * 
 */
namespace App\Utility;

/**
 * Class: Utility, 一般性工具程式,所有程式應以靜態方法撰寫與呼叫
 */
class Utility
{
    /**
     * 取得使用者ip
     * 來源：http://www.jaceju.net/blog/archives/1913/
     * @return string
     */
    public static function get_client_ip()
    {
        foreach (array(
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER)) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);
                    if ((bool) filter_var($ip, FILTER_VALIDATE_IP,
		    		                FILTER_FLAG_IPV4 |
                                    FILTER_FLAG_NO_RES_RANGE)) {  //FILTER_FLAG_NO_PRIV_RANGE |
                        return $ip;
                    }
                }
            }
        }
      return '0.0.0.0';
    }

    /**
     * 檢測給定IP是否在指定的範圍之內(只支援IPv4)
     * @param string $targetIp IPv4的IP
     * @param string $ipRange 單一IP或IP範圍, eg: IP範圍範例: "192.168.2.1-192.168.3.127", "192.168.2.10-20"
     * @return bool false if IP not in range or IP format invalid
     */
    public static function check_ip_in_limit(string $targetIp, string $ipRange)
    {
        // 檢查受測IP	
        if (filter_var($targetIp, FILTER_VALIDATE_IP) === false) {
            return false;
        }

        $targetNum = ip2long($targetIp);
        //完整IP
        if (preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $ipRange) && filter_var($ipRange, FILTER_VALIDATE_IP)) {
            return ($targetNum === ip2long($ipRange));
        }

        //完整區段IP
        if (preg_match('/^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\-(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})$/', $ipRange, $ipMatch) && filter_var($ipMatch[1], FILTER_VALIDATE_IP) && filter_var($ipMatch[2], FILTER_VALIDATE_IP)) {
            $fromNum = ip2long($ipMatch[1]);
            $toNum = ip2long($ipMatch[2]);
            return (max($fromNum, $toNum) >= $targetNum && min($fromNum, $toNum) <= $targetNum);
        }

        //底層區段 IP range
        if (preg_match('/^(\d{1,3}\.\d{1,3}\.\d{1,3}\.)(\d{1,3}\-\d{1,3})$/', $ipRange, $ipMatch)) {
            $range  = explode('-', $ipMatch[2]);
            $ipFrom = $ipMatch[1] . $range[0];
            $ipTo   = $ipMatch[1] . $range[1];
            if (filter_var($ipFrom, FILTER_VALIDATE_IP) && filter_var($ipTo, FILTER_VALIDATE_IP)) {
                $fromNum = ip2long($ipFrom);
                $toNum = ip2long($ipTo);
                return (max($fromNum, $toNum) >= $targetNum && min($fromNum, $toNum) <= $targetNum);
            }
        }
        return false;
    }

    /**
     * 轉換bytes (含單位)
     * @param int $bytes 位元組
     * @param int $precision 小數點位數
     * @return string 包含單位的值, 並依小數點位數來呈現, eg. "12 B", "512 KB", "110.55 GB"
     */
    public static function formatBytes(int $bytes, int $precision = 2)
    { 
		$units = array('B', 'KB', 'MB', 'GB', 'TB'); 
		$bytes = max($bytes, 0); 
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1); 
        $num = $bytes / pow(1024, $pow);
		return sprintf('%.*f %s', $precision, round($num, $precision), $units[$pow]);
    }

    /**
     * @deprecated v5.0, 請改用formatBytes()
     * 轉換bytes
     * @param int $bytes 位元組
     * @return 包含單位的值
     */
    public static function byteConvert($bytes)
    {
        $s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        $e = floor(log($bytes)/log(1024));
      
        return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
    }

    /**
     * 隨機產生密碼  
     * [2023/1/31]: 改使用random_int增加安全性; https://stackoverflow.com/questions/6101956/generating-a-random-password-in-php
     * @param int $length 密碼長度
     * @param string $keyspace 密碼字元組, 可依資安需求傳入不同的字元組
     * @return string 隨機密碼
     * @throws \UnexpectedValueException throw if keyspace chars length less than 2
     */
    public static function generator_password(int $length, string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $password = '';
        $max = mb_strlen($keyspace, '8bit') - 1; //即使字元組含有UTF-8字元一樣以byte計算
        if ($max < 1) {
            throw new \UnexpectedValueException('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; $i++) {
            $password .= $keyspace[random_int(0, $max)];
        }
        return $password;
    }

    /**
     * (待補)
     *
     * @param string $str
     * @param integer $split_len
     * @return array
     */
	public static function utf8_str_split($str, $split_len = 1)
	{
		if (!preg_match('/^[0-9]+$/', $split_len) || $split_len < 1) {
			return FALSE;
        }
		$len = mb_strlen($str, 'UTF-8');
		if ($len <= $split_len) {
			return array($str);
        }
        
		preg_match_all('/.{'.$split_len.'}|[^\x00]{1,'.$split_len.'}$/us', $str, $ar); 
		return $ar[0];
    }
    
    /**
     * 計算影像DPI
     * @param string $filename 檔案路徑
     * @return array
     */
    public static function get_dpi($filename)
    {
        $a = fopen($filename,'r');
        $string = fread($a,20);
        fclose($a);
  
        $data = bin2hex(substr($string,14,4));
        $x = substr($data,0,4);
        $y = substr($data,0,4);
  
        return array(hexdec($x),hexdec($y));
      } 

    /**
     * 將時間格式依指定類型做轉換
     * @param mixed $dateTime 有實作DateTimeInterface的object(eg: Dibi\DateTime) 或string
     * @param string $formatType long, medium, short, year, month, day 
     * @return string
     */
    public static function convertStandardDateTime($dateTime, $formatType = 'long')
    {
        if ($dateTime == null) {
            return '';
        }
        $timeFormat = 'Y-m-d H:i:s';
        switch ($formatType) {
            case 'long':
                break; //use default
            case 'medium':
                $timeFormat = 'Y-m-d H:i';
                break;
            case 'short':
                $timeFormat = 'Y-m-d';
                break;
            case 'year':
                $timeFormat = 'Y';
                break;
            case 'month':
                $timeFormat = 'm';
                break;
            case 'day':
                $timeFormat = 'd';
                break;
            default:
                break; //use default
        }

        if ($dateTime instanceof \DateTimeInterface) {
            $standardDateTime = date_format($dateTime, $timeFormat);
        } else if (is_string($dateTime)) {
            $dateTime = new \DateTime($dateTime, new \DateTimeZone('Asia/Taipei'));
            $standardDateTime = date_format($dateTime, $timeFormat);
        } else {
            $standardDateTime = '';
        }

        return $standardDateTime;
    }

    /**
     * 隨機產生UUID碼(v4) \
     * 出處: https://www.php.net/manual/zh/function.uniqid.php#94959
     * @return string UUID字串
     */
    public static function gen_uuid_v4()
    {
        //$localIP = gethostbyname(gethostname());
        //mt_srand(crc32(serialize([microtime(true), $localIP, 'ETC'])));
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    /**
     * Check string is valid UUIDv4 (不分大小寫)
     *
     * @param string|null $uuid UUID字串
     * @return bool
     */
    public static function isValidUUIDv4(?string $uuid)
    {
        if (!is_string($uuid) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i', $uuid) !== 1)) {
            return false;
        }
        return true;
    }

    /**
     * 將key-value array轉成xml格式
     * 
     * @param array $data key-value array
     * @param SimpleXMLElement $xml_data 需在呼叫程式中以new \SimpleXMLElement()來初值化並傳入
     */
    public static function array_to_xml($data, &$xml_data)
    {
        foreach ($data as $key => $value) {
            if (\is_numeric($key)) {
                $key = 'row';
            }
            if (\is_array($value)) {
                $subnode = $xml_data->addChild($key);
                Utility::array_to_xml($value, $subnode);
            } elseif($value != null) {
                $xml_data->addChild("$key",\htmlspecialchars("$value"));
            }
        }
    }

    /**
     * 將key-value array轉成csv \
     * **註**: 此函式會直接輸出outputBuffer的內容後關閉outputBuffer
     * 
     * @param array $data key-value array, 格式如下
     * [
     *     [
     *         'id' => 1,
     *         'title' => 'test1',
     *         'user' => 'BP Boy',
     *     ],[
     *         'id' => 2,
     *         'title' => 'test2',
     *         'user' => 'BP Girl',
     *     ],
     * ]
     * @param resource $outputBuffer 需在呼叫程式中以fopen("php://output",'w')來初值化
     */
    public static function array_to_csv($data, &$outputBuffer)
    {
        $count = count($data);
        for ($i = 0; $i < $count; $i++) {
            if ($i == 0) {
                //add header
                $header = array_keys($data[$i]);
                fputcsv($outputBuffer, $header);
            }
            $val = array_values($data[$i]);
            fputcsv($outputBuffer, $val);
        }
        fclose($outputBuffer);
    }

    /**
     * 將 Associative array 轉換成 CSV 的字串輸出, 含標題列
     * @param array $data 要轉換成 CSV 的二維關聯式陣列, 格式範例如下:
     * [
     *     [
     *         'id' => 1,
     *         'title' => 'test1',
     *         'user' => 'BP Boy',
     *     ],[
     *         'id' => 2,
     *         'title' => 'test2',
     *         'user' => 'BP Girl',
     *     ],
     * ]
     * @param string $delimiter = ',' 每個欄位間, 要用什麼符號間隔
     * @param string $enclosure = '"' 每個欄位要用什麼符號包住
     * @return string (csv data string)
     */
    public static function assoc_array_to_csv($data, $delimiter = ',', $enclosure = '"')
    {
        $csv = '';
        $first_row = true;
        foreach ($data as $row) {
            //加上標題列
            if ($first_row === true) {
                $first_element = true;
                $header = array_keys($row);
                foreach ($header as $element) {
                    // 除了第一個欄位外, 於 每個欄位 前面都需加上 欄位分隔符號
                    if (!$first_element) {
                        $csv .= $delimiter;
                    }
                    $first_element = false;
                    // CSV 遇到 $enclosure, 需要重複一次, ex: " => ""
                    $element = str_replace($enclosure, $enclosure . $enclosure, $element);
                    $csv .= $enclosure . $element . $enclosure;
                }
                $csv .= "\n";
            }

            $first_element = true;
            foreach ($row as $element) {
                // 除了第一個欄位外, 於 每個欄位 前面都需加上 欄位分隔符號
                if (!$first_element) {
                    $csv .= $delimiter;
                }
                $first_element = false;

                // CSV 遇到 $enclosure, 需要重複一次, ex: " => ""
                $element = str_replace($enclosure, $enclosure . $enclosure, $element);
                $csv .= $enclosure . $element . $enclosure;
            }
            $csv .= "\n";
            $first_row = false;
        }
        return $csv;
    }

    /**
     * 將 Array 轉換成 CSV 的字串輸出
     * array_to_csv(array $fields [, string $delimiter = ',' [, string $enclosure = '"' ]])
     * @ref http://php.net/manual/en/function.fputcsv.php
     * @param array $fields 要轉換成 CSV 的資料陣列
     * @param string $delimiter = ',' 每個欄位間, 要用什麼符號間隔
     * @param string $enclosure = '"' 每個欄位要用什麼符號包住
     * @return string (csv data string)
     * 
     The MIT License
     * @author: Tsung <tsunghao@gmail.com> https://blog.longwin.com.tw
     */
    public static function normal_array_to_csv($fields, $delimiter = ',', $enclosure = '"')
    {
        $csv = '';
        foreach ($fields as $field) {
            $first_element = true;

            foreach ($field as $element) {
                // 除了第一個欄位外, 於 每個欄位 前面都需加上 欄位分隔符號
                if (!$first_element) {
                    $csv .= $delimiter;
                }

                $first_element = false;

                // CSV 遇到 $enclosure, 需要重複一次, ex: " => ""
                $element = str_replace($enclosure, $enclosure . $enclosure, $element);
                $csv .= $enclosure . $element . $enclosure;
            }
            $csv .= "\n";
        }
        return $csv;
    }

    /**
     * transfer mulit-file upload array structure by index
     * @param $file_post 上傳檔案的結構
     */
	public static function reArrayFiles(&$file_post)
	{
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

    /**
     * 檢驗上傳是否成功,若有error發生會throw exception
     * @param $upload PHP的上傳物件
     */
    public static function checkUploadStatus($upload)
    {
        if (empty($upload)) {
            throw new \RuntimeException('無上傳檔案(No upload data)');
        } else {
            //check error status
            switch ($upload['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new \RuntimeException('無檔案(No file sent)');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \RuntimeException('超過檔案大小限制(Exceeded filesize limit)');
                default:
                    throw new \RuntimeException('檔案上傳錯誤，錯誤代碼:'.$upload['error']);
            }
        }
    }

    /**
     * remove folders and files
     */
    public static function rrmdir($dir)
    {
		if (is_dir($dir)) {
			$files = scandir($dir);
				foreach ($files as $file) { 
                    if ($file != "." && $file != "..") {
                        self::rrmdir("$dir/$file");
                    }
                }
				rmdir($dir);
		} elseif (file_exists($dir)) {
            unlink($dir);
        }
    }
    
    /**
     * Copy folders and files
     */
    public static function rcopy($src, $dst)
    {
        if (file_exists($dst)) {
            self::rrmdir($dst);
        }

        if (is_dir($src)) {
            mkdir($dst);
            $files = scandir($src);
            foreach ($files as $file) {
                if ($file != "." && $file != "..") {
                    self::rcopy("$src/$file", "$dst/$file");
                }
            }
        } elseif (file_exists($src)) {
            copy($src, $dst);
        }
    }

    /**
     * 將指定的key, value插入associative array的最前面
     * @param array $arr 要被插入的associative array
     */
    public static function array_unshift_assoc(&$arr, $key, $val) 
    { 
        $arr = array_reverse($arr, true); 
        $arr[$key] = $val; 
        $arr = array_reverse($arr, true); 
        return $arr;
    }

    /**
	 * ASCII 字元自動全形/半形轉換 (字碼補位法)
	 *
	 * @authro LIAO SAN-KAI
	 * 
	 * @param string $char 欲轉換的 ASCII 字元
	 * @param string $width 字形模式 half|full|auto (半形|全形|自動)
	 * @return string 轉換後的對應字元
	 */
    public static function shiftSpace($char=null, $width='auto')
    {
		//取得當前字元的16進位值
		$charHex = hexdec(bin2hex($char));

		//判斷當前字元為半形或全形
		$charWidth = ($char == '　' or ($charHex >= hexdec(bin2hex('！')) and $charHex <= hexdec(bin2hex ('～')))) ? 'full' : 'half';

		//如果字元字形與指定字形一樣，就直接回傳
		if($charWidth == $width) {
			return $char;
		}

		//如果是空白字元就直接比對轉換回傳
		if($char === '　' ) {
			return ' ';
		} elseif($char === ' ') {
			return '　';
		}

		//計算 ASCII 字元16進位的unicode差值
		$diff = abs(hexdec(bin2hex ('！')) - hexdec(bin2hex ('!')));

		//計算字元"_"之後的半形字元修正值(192)
		$fix = abs(hexdec(bin2hex ('＿')) - hexdec(bin2hex ('｀'))) - 1;

		//全形/半形轉換
		if($charWidth == 'full'){
			$charHex = $charHex - (($charHex > hexdec(bin2hex('＿'))) ? $diff + $fix : $diff); 
		} else {
			$charHex = $charHex + (($charHex > hexdec(bin2hex('_'))) ? $diff + $fix : $diff); 
		}

		return hex2bin(dechex($charHex));
    }
    
	/**
	 * CRC32 MAP TO 62CHR, Small sample convert crc32 to character map
	 * Based upon http://www.php.net/manual/en/function.crc32.php#105703
	 * (Modified to now use all characters from $map)
	 * (Modified to be 32-bit PHP safe)
	 */
	public static function khashCRC32($data)
	{
		static $map = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$hash = bcadd(sprintf('%u',crc32($data)) , 0x100000000);
		$str = "";
		do
		{
			$str = $map[bcmod($hash, 62) ] . $str;
			$hash = bcdiv($hash, 62);
		}
		while ($hash >= 1);
		return $str;
    }
    
    /**
     * @deprecated v5.0
     * 取得Windows系統的CPU使用量
     * 此函式在新版Windows上會有問題，請勿使用。由於不考慮修復故在之後版本淘汱
     */
    public static function get_server_cpu_usage_for_windows()
	{
		$_ENV['typeperfCounter'] = '\processor(_total)\% processor time';
		exec('typeperf -sc 1 "'.$_ENV['typeperfCounter'].'"', $p);
		$line = explode(',', $p[2]);
		$load = trim($line[1], '"');
		return $load;
	}

    /**
     * @deprecated v5.0
     * 取得Windows系統的記憶體使用量
     * 此函式使用過於受限，考慮在之後版本淘汱
     */
    public static function get_server_memory_usage_for_windows()
	{
		exec('wmic memorychip get capacity', $totalMemory);

		$sumMemory = 0;
		for($i=1; $i<count($totalMemory);$i++){
				$sumMemory = floatval($sumMemory) + floatval($totalMemory[$i]);
		}      
		$sumMemory = $sumMemory/1024/1024/1024;

		exec('wmic OS get FreePhysicalMemory /Value 2>&1', $output, $return);
		$freeMemory = substr($output[2],19);
		$freeMemory = $freeMemory/1024/1024;

		return (($sumMemory-$freeMemory)/$sumMemory)*100;
    }
    

    /**
     * @deprecated v5.0
     * 取得Windows系統的硬碟使用量
     * 此函式使用過於受限，考慮在之後版本淘汱
     */
	public static function get_server_disk_usage_for_windows()
	{
		$diskPathList = array('C:', 'D:');
		$diskTotalSpace = 0;
		$diskFreeSpace = 0;
		
		foreach ($diskPathList as $disk) {
			if (file_exists($disk)) {
				$diskTotalSpace += disk_total_space($disk);
				$diskFreeSpace += disk_free_space($disk);
			}
		}

		return ($diskTotalSpace - $diskFreeSpace)/ $diskTotalSpace * 100;
    }
    
    /**
     * 取得當前時間到微秒(10^-6)
     * @param string $format 輸出時間的格式,預設Y-m-d H:i:s.u
     * @return string
     */
    public static function getDateTimeWithMicrosecond(string $format='Y-m-d H:i:s.u')
    {
        $now = \DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        return $now->format($format);
    }

    /**
     * 檢查欄位必填
     * @param array $param : 要被檢查的欄位 key & value
     * @param array $required_columns : 必填欄位的keys
     * @return array 檢查結果, eg. ['success' => true, 'message' = '']
     */
    public static function checkRequired($param, $required_columns)
    {
        $check = array('success' => false,'message' => '');
        foreach ($required_columns as $column) {
            if (!isset($param[$column])) {
                $check['message'] = 'parameter: <'.$column.'> is required';
                return $check;
            }

            if (is_string($param[$column]) && trim($param[$column])=='') {
                $check['message'] = 'parameter: <'.$column.'> is empty';
                return $check;
            }

            if (is_array($param[$column]) && empty($param[$column])) {
                $check['message'] = 'parameter: <'.$column.'> has no element';
                return $check;
            }
        }

        $check['success'] = true;
        return $check;
    }

    /**
     * 回傳 dibi sort
     * @param array $param 'sort'排序欄位，多組時可以,分隔
     * @param string $default_field 排序預設欄位
     * @param string $default_order 排序預設欄位的方向 asc|desc
     * @return array  
     */
    public static function createQuerySort(array $param=[], string $default_field='sno', string $default_order='asc')
    {
        if(!isset($param['sort'])||trim($param['sort'])=='')
        {
            return [
                $default_field => (strtolower($default_order)=='desc')? false: true
            ];
        }
        
        $arr = array();
        $sorts = explode(',',$param['sort']);
        foreach ($sorts as $oneSort) 
        {
            $tmp = explode("_" , $oneSort);
            $order = array_pop($tmp);
            $field = implode("_", $tmp);
            $arr[$field] = $order == 'asc' ? true : false;
        }
        return $arr;
    }

    /**
     * 回傳 dibi offset
     * @param int $page_num
     * @param int $page_limit
     * @return int
     */
    public static function createQueryOffset(int $page_num, int $page_limit)
    {
        $page_num = isset($page_num) && $page_num >= 1 ? $page_num : 1;
        $page_limit = isset($page_limit) && $page_limit >= 1 ? $page_limit : 10;
        return ($page_num - 1) * $page_limit;
    }


    /**
     * 回傳 dibi 分頁資訊並考慮 page_limit 可能為 null (不分頁全部回傳) 的狀態
     * 無法區分是真的沒寫還是寫null => 沒寫就是 null (不分頁)
     * 如果預設要20 務必只能寫明 $param['page_limit'] = 20
     * @param array $param
     */
    public static function createQueryLimit(array $param=[])
    {
        return (isset($param['page_limit'])&&is_numeric($param['page_limit']))? (int)$param['page_limit']: null; 
        // return (is_numeric($param['page_limit'])||is_null($param['page_limit']))? $param['page_limit']: 20;
    }

    public static function createQueryPagenum(array $param=[])
    {
        return isset($param['page_num'])? (int)$param['page_num']: 1; // not allow null
    }

    public static function createQueryOffset2(array $param=[])
    {
        $page_limit = self::createQueryLimit($param);
        $page_num   = self::createQueryPagenum($param);
        return ($page_limit==null)? 0 : ($page_num-1)*$page_limit;
    }

    /**
     * 回傳 dibi SQL Update 用的array
     * @param array $param
     * @param array $available_columns 可能被update的欄位
     * @return array
     */
    public static function createUpdateSet($param, array $available_columns = [])
    {
        $updateSets = array();

        foreach ($available_columns as $field) {
            if (isset($param[$field]) && trim($param[$field]) !== '') {
                $updateSets[$field] = $param[$field];
            }
        }
        return $updateSets;
    }

    /**
     * 民國 yyymmdd 轉 西元 YYYY-mm-dd
     * @param string $ROCDate 民國日期 yyymmdd
     * @return string 西元日期 YYYY-mm-dd
     */
    public static function toWesternDate($ROCDate)
    {
        $dd = substr($ROCDate, -2);
        $mm = substr($ROCDate, -4, -2);
        $ROCYear = substr($ROCDate, 0, -4);
        return ((int)$ROCYear+1911).'-'.$mm.'-'.$dd;
    }

    /**
     * @param string $WesternDate 西元日期 YYYY-mm-dd 
     * @return string 民國日期 yyymmdd
     */
    public static function toROCDate($WesternDate)
    {
        $tmp = explode('-', $WesternDate);
        $AD = (int)$tmp[0] - 1911;
        return $AD.$tmp[1].$tmp[2];
    }

    /**
     * 四位時間加冒號 ex: 0830 -> 08:30
     */
    public static function timeAddColon($time)
    {
        return substr($time,0,2).':'.substr($time,2,4);
    }

    /**
     * Download the $filePath
     * @deprecated
     * @param $response
     * @param string $filePath 檔案位置
     * @param string $basename (optional)下載檔案名稱
     * @return void
     */
    public static function getDownloadResponse($response, $filePath, $basename = '')
    {
        if ($basename == null || $basename == '') {
            $basename = pathinfo($filePath, PATHINFO_BASENAME);
        }

        if (file_exists($filePath)) {
            $response = $response->withHeader('Content-Disposition', 'attachment;filename=' . rawurlencode($basename) . ';filename*=UTF-8' . "''" . rawurlencode($basename))
                ->withHeader('Content-Type', 'application/octet-stream')
                ->withHeader('Content-Description', 'File Transfer')
                ->withHeader('Expires', '0')
                ->withHeader('Cache-Control', 'must-revalidate')
                ->withHeader('Pragma', 'public')
                ->withHeader('Content-Length', filesize($filePath));
            readfile($filePath);
            return $response;
        } else {
            // $this->setServerFailApiResult('File not exist');
            return $response->withJson(
                array(
                    'success' => false,
                    'code' => 500,
                    'message' => "File (" . $filePath . ") not exist"
                ),
                200,
                JSON_UNESCAPED_UNICODE
            );
        }
    }

    /**
     * 產生指定長度的隨機字串
     * **僅供產生測試資料用,勿用來產生隨機密碼
     *
     * @param integer $length 字串長度
     * @param string $characterPool (optional) 指定隨機字串池中的字串,若不給則為預設值
     * @return string
     */
    public static function generateRandomString($length = 10, $characterPool='')
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($characterPool != null) {
            $characters = $characterPool;
        }

        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * 用在陣列式的檔案路徑設定轉換
     *
     * @param array|string $filePath 檔案路徑,可為陣列或字串
     * @param string $defaultPath 預設路徑,若檔案路徑為空或其他格式時,回傳此值
     * @return string 檔案路徑
     */
    public static function convertFilePathArrayToString($filePath, string $defaultPath='')
    {
        if (empty($filePath)) { //同時判斷空陣列,null與空字串
            return $defaultPath;
        } elseif (is_string($filePath)) {
            return $filePath;
        } elseif (is_array($filePath)) {
            return DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $filePath);
        } else {
            return $defaultPath;
        }
    }

    /**
     * 組合檔案路徑並避免Path Traversal
     *
     * @param string $basePath 檔案路徑上層根目錄
     * @param string $inputPath 輸入的檔案路徑
     * @param boolean $addDirSeparator 是否在根目錄與檔案路徑間增加區隔符
     * @return string|false 組合好的檔案路徑, false if Path Traversal
     */
    public static function safeCombineFilePath(string $basePath, string $inputPath, bool $addDirSeparator=false) 
    {
        //check path include ".."
        if (strpos($inputPath, '..') !== false) {
            return false;
        }
        //check other way to traversal
        $realBasePath = realpath($basePath);
        $dirSeparator = $addDirSeparator ? DIRECTORY_SEPARATOR : '';
        $inputFullPath = sprintf('%s%s%s', $basePath, $dirSeparator, $inputPath);
        $realInputFullPath = realpath($inputFullPath);
        if ($realInputFullPath === false || strpos($realInputFullPath, ($realBasePath . DIRECTORY_SEPARATOR)) !== 0) {
            return false;
        } else {
            return $realInputFullPath;
        }
    }

    /**
     * 過濾輸入值為整數,
     *
     * @param mixed $inputNum 要過濾的輸入值
     * @param int $defaultNum 若輸入值非整數時回傳預設值
     * @return int
     */
    public static function filterInputAsInteger($inputNum, int $defaultNum)
    {
        $filterNum = filter_var($inputNum, FILTER_VALIDATE_INT);
        return ($filterNum === false) ? $defaultNum : $filterNum;
    }

    /**
     * 取得指定日期的"隔日"0點0分0秒
     *
     * @param string $datetimeString 日期,預
     * @return string|false 隔日0點0分0秒, 若轉換失敗則為false
     */
    public static function getNextDayBeginning(string $datetimeString='now')
    {
        $timestamp = strtotime($datetimeString . ' +1 day');
        return ($timestamp === false) ? false : date('Y-m-d 00:00:00', $timestamp);
    }

    /**
     * 將 Associative array 轉換成 CSV 的字串輸出, 含標題列; 為assoc_array_to_csv的生成器版本
     * @param \Generator $genData 要轉換成 CSV 的Associative array資料生成器
     * @param string $delimiter = ',' 每個欄位間, 要用什麼符號間隔
     * @param string $enclosure = '"' 每個欄位要用什麼符號包住
     * @return \Generator 回傳生成器
     */
    public static function assoc_array_to_csv_generator(\Generator $genData, $delimiter = ',', $enclosure = '"')
    {
        $first_row = true;
        foreach ($genData as $row) {
            //加上標題列
            if ($first_row === true) {
                $first_element = true;
                $header = array_keys($row);
                foreach ($header as $element) {
                    // 除了第一個欄位外, 於 每個欄位 前面都需加上 欄位分隔符號
                    if (!$first_element)
                        yield $delimiter;
                    $first_element = false;
                    // CSV 遇到 $enclosure, 需要重複一次, ex: " => ""
                    $element = str_replace($enclosure, $enclosure . $enclosure, $element);
                    yield $enclosure . $element . $enclosure;
                }
                yield "\n";
            }

            $first_element = true;
            foreach ($row as $element) {
                // 除了第一個欄位外, 於 每個欄位 前面都需加上 欄位分隔符號
                if (!$first_element)
                    yield $delimiter;

                $first_element = false;

                // CSV 遇到 $enclosure, 需要重複一次, ex: " => ""
                $element = str_replace($enclosure, $enclosure . $enclosure, $element);
                yield $enclosure . $element . $enclosure;
            }
            yield "\n";
            $first_row = false;
        }
    }
}