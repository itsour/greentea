<?php 
/**
 * 實作FileServerConnection
 */
namespace App\Module\FileServer;

use App\Core\Config\Config;
use App\Core\Logger\FileLogger;
use App\Utility\Utility;

/**
* Connect File Server based on config setting, especially for client is Windows
*/
class FileServer implements FileServerConnection
{
    private static $fileServer;
    private static $fsPath;
    private static $fsUser;
    private static $fsPassword;
    private static $logFullPath;
    private static $writeLog;
    
    private static $_uniqueInstance = '';

    /**
     * 確認與file server的連線並回傳File system的路徑
     *
     * @param string $configSetName system.json中的config的設定名稱, eg:file_server
     * @return string|boolean 若連線成功,回傳file system路徑, 若失敗回傳false
     */
    public static function connect($configSetName='file_server')
    {
        if (null != static::$_uniqueInstance) {
            return static::$_uniqueInstance;
        }

        $configSetName = ($configSetName == null) ? 'file_server' : $configSetName;
        $env = Config::_getEnv('system');
        
        self::$fileServer = $env[$configSetName]['file_system_server'] ?? '';
        self::$fsPath = Utility::convertFilePathArrayToString($env[$configSetName]['file_system_root_path'] ?? '');
        self::$fsUser = $env[$configSetName]['file_system_user'] ?? '';
        self::$fsPassword = $env[$configSetName]['file_system_password'] ?? '';
        self::$writeLog = $env[$configSetName]['write_log'] ?? false;

        $logFileName = 'file_server.log';
        if (isset($env['file_log']['log_folder_path'])) {
            $logFolder =  Utility::convertFilePathArrayToString($env['file_log']['log_folder_path']);
            self::$logFullPath = $logFolder . DIRECTORY_SEPARATOR . $logFileName;
        }
        
        $clientOS = $env[$configSetName]['client_type'] ?? 'windows';

        try {
            if (null == self::$fsPath) {
                throw new \Exception('File path is empty, please set system.'. $configSetName . '.file_system_root_path');
            }
            if (strtolower($clientOS) == 'windows') {
                self::connectFileServerFromWindows();
                static::$_uniqueInstance = self::$fsPath;
            } elseif(strtolower($clientOS) == 'linux') {
                static::$_uniqueInstance = self::$fsPath;
            } else {
                static::$_uniqueInstance = self::$fsPath;
            }
        } catch (\Exception $e) {
            if (true === self::$writeLog) {
                FileLogger::error($e->getMessage(), [], $logFileName, __METHOD__);
            }
            static::$_uniqueInstance = false;
        }

        return static::$_uniqueInstance;
    }

    /**
     * client為Windows, 透過指令連到File Server
     */
    private static function connectFileServerFromWindows()
    {
        if (file_exists(self::$fsPath)) {
            return;
        }

        if (true === self::$writeLog) {
            $cmd = sprintf('net use %s "\\\\%s" %s /user:%s /persistent:no >> "%s" 2>&1', self::$fsPath, self::$fileServer, self::$fsPassword, self::$fsUser, self::$logFullPath);
        } else {
            $cmd = sprintf('net use %s "\\\\%s" %s /user:%s /persistent:no', self::$fsPath, self::$fileServer, self::$fsPassword, self::$fsUser);
        }
        system($cmd);

        //test connect again, if not exist, throw exception
        if (!file_exists(self::$fsPath)) {
            throw new \RuntimeException('Connect file server fail.');
        }
    }
}