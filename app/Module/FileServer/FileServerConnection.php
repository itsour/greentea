<?php 
/**
 * 檔案伺服器連線介面
 */
namespace App\Module\FileServer;

/**
 * FileServerConnection interface
 */
interface FileServerConnection
{
    /**
     * 確認與file server的連線並回傳File system的路徑
     *
     * @param string $configSetName system.json中的config的設定名稱, eg:file_server
     * @return string|boolean 若連線成功,回傳file system路徑, 若失敗回傳false
     */
    public static function connect($configSetName);
}