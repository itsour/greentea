<?php
namespace App\Module\JwtTool;

use \Firebase\JWT\JWT;
use App\Core\Config\Config;

class JwtTool
{
    static public function encode($userInfo) {
        $jwt = (new Config())->getConfig('jwt.jwt');
        $jwtToken = JWT::encode($userInfo, $jwt['secret'], $jwt['algorithm'][0]);
        return $jwtToken;
    }
}
