<?php
namespace App\Module\Office;

use App\Module\Office\ExcelModuleImpl;
use App\Module\Office\Interfaces\OfficeInterface;

class OfficeFactory
{
    public static function createOfficeModule($extension, $filename): OfficeInterface
    {
        $type = strtolower($extension);
        switch ($type) {
            case 'xlsx':
            case 'xlx':
            case 'xlsb':
                return new ExcelModuleImpl($extension, $filename);
            default:
                throw new \Exception($extension . ' 不在工廠方法支援清單內。');
        }
    }
}
