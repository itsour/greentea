<?php
namespace App\Module\Office;

abstract class OfficeBaseModule
{
    protected $filename;
    protected $extension;

    protected $supportExtension = [];

    protected $nonSupportMessage = "";

    /** Init user input to self var */
    public function __construct(string $extension, string $filename)
    {
        $this->extension = strtolower($extension);
        $this->verifyExtension();
        $this->buildFilename($filename);
    }

    /** check user input extension is support and throw exception message */
    protected function verifyExtension()
    {
        if (!\in_array($this->extension, $this->supportExtension)) {
            throw new \Exception($this->nonSupportMessage);
        }
    }

    /** build final filename, filename contains extension */
    protected function buildFilename($filename)
    {
        if ($filename === '' || is_null($filename)) {
            $filename = time();
        }

        $tmp = array($filename, $this->extension);
        $this->filename = implode(".", $tmp);
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getExtension()
    {
        return $this->extension;
    }
}
