<?php
namespace App\Module\Office;

use Box\Spout\Common\Type;
use Box\Spout\Common\Entity\Style\Color;
// use Box\Spout\Writer\AbstractWriter;
use Box\Spout\Writer\Common\Creator\WriterFactory;
use Box\Spout\Writer\WriterInterface;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use App\Module\Office\Interfaces\OfficeInterface;
use App\Exception\NotImplementedException;

/**
 * Ods instance.
 * @deprecated Due to Box\Spout is abandoned, this class will deprecated in MarkII v5
 */
class Ods implements OfficeInterface
{
    private $fileName;
    private $writer;
    private $writerOds;
    private $filePath;
    
    public function __construct()
    {
        // $this->init();
        // $this->example();
        // $this->download();
    }

    public function init(string $fileName) 
    {
        $this->fileName  = (null !== $fileName) ? $fileName : time();
        $this->fileName .= $this->fileName.'.ods';

        $this->writer = WriterFactory::createFromType(Type::ODS);

        // $writer->openToFile("./ttttttttttt.ods"); // write data to a file or to a PHP stream
        $this->writer->openToBrowser($this->fileName); // stream data directly to the browser
        
    }

    public function addRow($arrContent = []) 
    {
        $this->writer->addRow($arrContent);
    }

    public function download($input=null) 
    {
        $this->writer->close();
    }

    public function example() 
    {
        $firstRow = ['','新聞標題', '新聞內文', '發佈時間', '類型', '媒體', '作者', '輿情聲量'];
        $firstRow_style = (new StyleBuilder())
               ->setFontBold()
               ->setFontSize(12)
               ->setFontName('微軟正黑體')
               ->setFontColor(Color::BLACK)
               // ->setBackgroundColor(Color::YELLOW) // or RGB
               ->setShouldWrapText()
               ->build();

        $this->addRow($firstRow);
        // $writer->addRows($multipleRows); // add multiple rows at a time

        $newsRow   = array();
        $newsRow[] = "1";
        $newsRow[] = "Test";
        $newsRow[] = "Test1";
        $newsRow[] = "Test2";
        $newsRow[] = "Test3";
        $newsRow[] = "Test4";
        $newsRow[] = "Test5";
        $newsRow[] = "Test6";

        $this->addRow($newsRow);
        
        $this->download();
    }

    public function saveToFile($filePath)
    {
        //由於OfficeInterface的改變必須加上實作此function
        //但目前尚未能實作，故先丟出exception
        throw new NotImplementedException('The method not implement', 'The method not implement');
    }

    public function setContent(array $content)
    {
        //由於OfficeInterface的改變必須加上實作此function
        //但目前尚未能實作，故先丟出exception
        throw new NotImplementedException('The method not implement', 'The method not implement');
    }
}


