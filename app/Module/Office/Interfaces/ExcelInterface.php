<?php
namespace App\Module\Office\Interfaces;

/**
* Interface for Excel Special Action
*/
interface ExcelInterface
{
    /**
     * Setting Excel Default Style
     * @return void
     */
    public function initStyle();

    /**
     * Setting Excel MAX Sheet Index, Default Sheet Count = 1
     * @return void
     */
    public function setMAXCountSheet(int $maxIndex);

    /**
     * get Excel MAX Sheet Index
     * @return void
     */
    public function getSheetCount();

    /**
     * Setting Excel Current Sheet index, Index Start with Zero. Jusk Like Array Index.
     * If Param Sheet Index More Then Max Sheet Index, This Module Auto Create
     * @return void
     */
    public function setCurrentSheetIndex(int $sheetIndex);

    /**
     * Setting Excel pageName/perPage
     * @return void
     */
    public function setAllPageName(array $pageNameSetting);

    /**
     * get Excel active Sheet index
     * @return void
     */
    public function getCurrentSheetIndex();

    /**
     * Setting Excel pageContent After Set Active Sheet Index
     * @return void
     */
    public function setPageContent(array $pageContent);

    /**
     * Setting Excel Current Page Title
     * @return void
     */
    public function setPageName(string $pageName);

    /**
     * Getting Excel Current Page Title
     * @return void
     */
    public function getPageName();
}
