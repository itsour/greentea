<?php
namespace App\Module\Office\Interfaces;

/**
* Interface for Excel, Pdf, Ods
*/
interface OfficeInterface
{
    /**
     * Setting default filename, extension value of Excel, Pdf or Ods.
     * @param $extension file extension
     * @param $fileName local fileanme or header content for download. if empty is true, filename set to time()
     * @return void
     */
    // public function init($extension, $fileName = '');

    /**
     * Return file for Download
     * @return void
     */
    public function download();

    /**
     * Save output to local file
     * @return void
     */
    public function saveToFile($filePath);

    /**
     * Set content to class buildin var
     * @return void
     */
    public function setContent(array $content);
}
