<?php 
namespace App\Module\Office;

use App\Module\Office\Interfaces\OfficeInterface;
use App\Exception\NotImplementedException;

/**
* TCPdf instance
*/
class Pdf extends \TCPDF implements OfficeInterface
{
    private $fileName;
    
    public function __construct()
    {
        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $this->init();
        // $this->download($this->example());
    }

    public function init(string $fileName, $fontType='cid0jp', $fontSize=12) 
    {
        $this->fileName  = (null !== $fileName) ? $fileName : time();
        $this->fileName .= $this->fileName.'.pdf';

        $this->setPrintHeader(false); //不要頁首
        $this->setPrintFooter(false); //不要頁尾
        
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); //PDF_FONT_MONOSPACED

        // set margins
        $this->SetMargins(10, 10, 10);
        // $this->SetHeaderMargin(PDF_MARGIN_HEADER);
        // $this->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks 自動分頁
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // ---------------------------------------------------------
        // set default font subsetting(子集) mode
        // $this->setFontSubsetting(true);
        $this->SetFont($fontType, '', $fontSize, '', true); // msungstdlight for 繁體中文
        // ---------------------------------------------------------

        $this->AddPage();
    }

    public function download($input=null) 
    {
        $html = $input;

        ob_end_clean();
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $this->Output($this->fileName, 'I');
    }

    public function example() 
    {
        $html  = "";
        $html .= '<table style="margin-bottom: 20px; padding:5px;">';
        $html .= '<tr><th style="color:green;"><h3>LiuGod is super God!</h3></th></tr>';

        $html .= '<tr><td style="color:#666666;">LiuGod will maka America great again!</td></tr>';

        $html .= '<tr><td>';
        $html .= '<p><span style="color: #007799;">【發佈時間】：</span>2016-12-14';
        $html .= '<br><span style="color: #007799;">【類型】：</span>God Message';
        $html .= '<br><span style="color: #007799;">【媒體】：</span>LiuGod Media';
        $html .= '<br><span style="color: #007799;">【作者】：</span>LiuGod';

        $html .= '</p></td></tr>';

        $html .= "</table>";
        return $html;
    }

    public function saveToFile($filePath)
    {
        //由於OfficeInterface的改變必須加上實作此function
        //但目前尚未能實作，故先丟出exception
        throw new NotImplementedException('The method not implement', 'The method not implement');
    }

    public function setContent(array $content)
    {
        //由於OfficeInterface的改變必須加上實作此function
        //但目前尚未能實作，故先丟出exception
        throw new NotImplementedException('The method not implement', 'The method not implement');
    }
}


