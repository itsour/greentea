<?php

namespace App\Module\Office;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use App\Module\Office\Interfaces\OfficeInterface;
use App\Module\Office\Interfaces\ExcelInterface;
use App\Module\Office\OfficeBaseModule;

/**
* PHPExcel instance
*/
class ExcelModuleImpl extends OfficeBaseModule implements OfficeInterface, ExcelInterface
{
    public $sheet;
    private $sheetIndex;
    private $content;

    private $defaultContentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    private $defualtFontSize = 12;
    private $defaultFont = '微軟正黑體';
    private $defHorizontalAlignment = Alignment::HORIZONTAL_LEFT;
    private $defVerticalAlignment = Alignment::VERTICAL_CENTER;

    protected $supportExtension = ['xlsx', 'xls', 'xlsb'];

    protected $nonSupportMessage = "本模組只支援 xlsx(After 2007), xlsb(After 2007 Binrary), xls(Behind 2007)";

    public function __construct(string $extension = 'xlsx', string $filename = '')
    {
        @parent::__construct($extension, $filename);

        $this->sheet = new Spreadsheet();
        $this->initStyle();
    }

    public function initStyle()
    {
        $this->sheet->getDefaultStyle()->getFont()->setName($this->defaultFont);
        $this->sheet->getDefaultStyle()->getFont()->setSize($this->defualtFontSize);
        $this->sheet->getDefaultStyle()->getAlignment()->setHorizontal($this->defHorizontalAlignment);
        $this->sheet->getDefaultStyle()->getAlignment()->setVertical($this->defVerticalAlignment);
    }

    /**
     * Get sheet count
     *
     * @return int
     */
    public function getSheetCount()
    {
        return $this->sheet->getSheetCount();
    }

    public function setMAXCountSheet(int $maxSheetCount)
    {
        $sheetRowCount = $this->getSheetCount();
        if ($maxSheetCount > $sheetRowCount) {
            $startSheetIndex = $sheetRowCount;
            $maxSheetIndex = ($maxSheetCount - 1);

            $this->createSheetIndex($startSheetIndex, $maxSheetIndex);
        }
    }

    private function createSheetIndex($startIndex, $endIndex)
    {
        for ($i = $startIndex; $i <= $endIndex; $i++) {
            $this->sheet->createSheet($i);
        }
    }

    public function setCurrentSheetIndex(int $sheetIndex)
    {
        $maxIndex = $this->getSheetCount();
        if ($sheetIndex > ($maxIndex - 1)) {
            $this->createSheetIndex($maxIndex, $sheetIndex);
        }

        $this->sheetIndex = $sheetIndex;
        $this->sheet->setActiveSheetIndex($this->sheetIndex);
    }

    /**
     * 寫入多個分頁(sheet)的內容
     *
     * @param array $content 要寫入的內容，需為3維陣列: "分頁" -> "列" -> "行"
     * @param bool $strictNullComparison 是否嚴謹看待null值。若為false則輸入0會被視為空值
     * @return void
     */
    public function setContent(array $content, bool $strictNullComparison = true)
    {
        $this->verifyInputContent($content);

        $this->content = $content;

        foreach ($this->content as $sheetIndex => $pageContent) {
            if ($sheetIndex != 0) {
                $this->sheet->createSheet();
            }

            $this->setCurrentSheetIndex($sheetIndex);
            $this->setPageContent($pageContent, $strictNullComparison);
        }
    }

    private function verifyInputContent(array $content)
    {
        if (!isset($content[0]) || \gettype($content[0]) !== 'array') {
            throw new \Exception('excel content 需採用三維陣列。針對 "分頁" -> "列" -> "行"');
        }

        if (\gettype($content[0][0]) !== 'array') {
            throw new \Exception('excel content 需採用三維陣列。針對 "分頁" -> "列" -> "行"');
        }
    }

    public function getCurrentSheetIndex()
    {
        return $this->sheetIndex;
    }

    /**
     * 將內容寫入特定的sheet之中
     *
     * @param array $pageContent 要寫入的內容，需為2維陣列: "列" -> "行"
     * @param boolean $strictNullComparison 是否嚴謹看待null值。若為false則輸入0會被視為空值
     * @return void
     */
    public function setPageContent(array $pageContent, bool $strictNullComparison = true)
    {
        if (is_null($this->sheetIndex)) {
            throw new \Exception(
                'Sheet Index not Set. You Must Call setCurrentSheetIndex'
            );
        }

        $this->content[$this->sheetIndex] = $pageContent;
        $this->sheet->getActiveSheet()->fromArray($pageContent, null, 'A1', $strictNullComparison);
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setAllPageName(array $pageNameSetting)
    {
        foreach ($pageNameSetting as $sheetIndex => $pageName) {
            $this->sheet->setActiveSheetIndex($sheetIndex);
            $this->sheet->getActiveSheet()->setTitle($pageName);
        }
    }

    public function setPageName(string $pageName)
    {
        $this->sheet->getActiveSheet()->setTitle($pageName);
    }

    public function getPageName()
    {
        return $this->sheet->getActiveSheet()->getTitle();
    }

    public function download()
    {
        $this->setDownloadHeader();
        $this->output('php://output');
    }

    private function setDownloadHeader()
    {
        header('Content-Type: ' . $this->defaultContentType);
        header('Content-Disposition: attachment; filename="' . $this->filename . '"');
    }

    public function saveToFile($specifyPath)
    {
        $tmp = array($specifyPath, $this->filename);
        $target = implode(DIRECTORY_SEPARATOR, $tmp);
        $this->output($target);
    }

    private function output($target)
    {
        $writer = IOFactory::createWriter($this->sheet, ucfirst($this->extension));
        $writer->save($target);
    }
}
