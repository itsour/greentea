<?php
namespace App\Module\FormatExport;

use App\Utility\Utility;

require_once __DIR__ . "/../../../vendor/autoload.php";

/**
 * 基本csv匯出,使用生成器
 */
class BasicCsvExportWithGenerator implements FormatExportInterface
{
    /**
     * 下載成csv
     *
     * @param \Generator $content 用來產生資料的生成器
     * @param string $fileName 下載的檔案名稱
     * @return void
     */
    public function download($content, string $fileName='')
    {
        $fileName = ExportTool::getFileName('csv',$fileName);
        $genOutput = Utility::assoc_array_to_csv_generator($content, ',', '"');
        
        ExportTool::setHeader('csv',$fileName);
        $i = 0;
        foreach ($genOutput as $str) {
            if ($i === 0) {
                //為了excel UTF8的呈現,加上BOM
                echo "\xEF\xBB\xBF";
            }
            echo $str;
            $i++;
        }
    }
}