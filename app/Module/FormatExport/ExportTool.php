<?php
namespace App\Module\FormatExport;

require_once __DIR__ . "/../../../vendor/autoload.php";

/**
 * 匯出模組工具
 */
class ExportTool 
{
    public static function setHeader(string $format,$fileName)
    {        
        ob_clean();
        switch ($format) {
            case 'csv':
            case 'CSV':
                header("Content-type: application/csv; charset=UTF-8");
                break;
            case 'json':
            case 'JSON':
                header('Content-type: application/json; charset=UTF-8');
                break;
            case 'xml':
            case 'XML':
                header('Content-type: text/xml; charset=UTF-8');
                break;
            default:
                header('Content-type: text/html; charset=UTF-8');
                break;
        }
        header('Content-Disposition', 'attachment;filename=' . rawurlencode($fileName) . ';filename*=UTF-8' . "''" . rawurlencode($fileName));
    }


    public static function getFileName(string $format,$fileName)
    {
        if (null == $fileName) {
            $fileName = "download_$format";
        }
        $nameArr = explode('.', $fileName);
        if ($format != array_pop($nameArr)) {
            $fileName .= ".$format";
        }
        return $fileName;
    }
}