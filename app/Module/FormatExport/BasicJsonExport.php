<?php
namespace App\Module\FormatExport;

use App\Utility\Utility;

require_once __DIR__ . "/../../../vendor/autoload.php";

/**
 * 基本json匯出
 */
class BasicJsonExport implements FormatExportInterface
{
    /**
     * 下載成json
     *
     * @param array $content 要匯出的內容
     * @param string $fileName 下載的檔案名稱
     * @return void
     */
    public function download($content, string $fileName='')
    {
        $fileName = ExportTool::getFileName('json', $fileName);

        ExportTool::setHeader('json', $fileName);
        echo json_encode($content, JSON_UNESCAPED_UNICODE);
    }
}