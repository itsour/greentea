<?php
namespace App\Module\FormatExport;

interface FormatExportInterface
{
    /**
     * 下載檔案
     *
     * @param array|\Generator $content 要被下載的資料,亦可為生成器
     * @param string $fileName 下載的檔案
     * @return void
     */
    public function download($content, string $fileName='');
}