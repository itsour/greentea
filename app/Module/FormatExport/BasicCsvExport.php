<?php
namespace App\Module\FormatExport;

use App\Utility\Utility;

require_once __DIR__ . "/../../../vendor/autoload.php";

/**
 * 基本csv匯出
 */
class BasicCsvExport implements FormatExportInterface
{
    public function download($content, string $fileName='')
    {
        $fileName = ExportTool::getFileName('csv',$fileName);
        // $fileName = $this->getFileName($fileName);

        //為了excel UTF8的呈現,加上BOM
        $output = "\xEF\xBB\xBF";
        $output .= Utility::assoc_array_to_csv($content, ',', '"');
        ExportTool::setHeader('csv',$fileName);
        echo $output;
    }

}