<?php
namespace App\Module\FormatExport;

use App\Utility\Utility;

require_once __DIR__ . "/../../../vendor/autoload.php";

/**
 * 基本json匯出,使用生成器
 */
class BasicJsonExportWithGenerator implements FormatExportInterface
{
    /**
     * 下載成json
     *
     * @param \Generator $content 用來產生資料的生成器
     * @param string $fileName 下載的檔案名稱
     * @return void
     */
    public function download($content, string $fileName='')
    {
        $fileName = ExportTool::getFileName('json',$fileName);

        ExportTool::setHeader('json',$fileName);
        //build json string then output
        echo '[';
        $firstData = true;
        while ($content->valid()) {
            $row = $content->current();
            if (empty($row)) {
                $content->next();
                continue;
            }
            //若有下一筆,需加上逗號分隔
            if ($firstData!==true) {
                echo ',';
            }
            $jsonStr = json_encode($row, JSON_UNESCAPED_UNICODE);
            echo $jsonStr;
            $firstData = false;
            $content->next();
        }
        echo ']';
    }

}