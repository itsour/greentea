<?php
namespace App\Module\FormatExport;

use App\Utility\Utility;

require_once __DIR__ . "/../../../vendor/autoload.php";

/**
 * 資料集匯出成xml
 */
class DatasetXmlExport implements FormatExportInterface
{
    /**
     * 下載成xml
     *
     * @param array $content 要匯出的資料內容
     * @param string $fileName 下載的檔案名稱
     * @return void
     */
    public function download($content, string $fileName='')
    {
        $fileName = ExportTool::getFileName('xml',$fileName);
        
        ExportTool::setHeader('xml',$fileName);
        //build xml string then output
        echo '<?xml version="1.0" encoding="UTF-8" ?>';
        echo '<nodes>';
        foreach ($content as $row) {
            if (empty($row)) {
                continue;
            }
            echo '<node>';
            foreach ($row as $k => $v) {
                echo '<' . $k . '>';
                $v = html_entity_decode($v);
                $v = htmlspecialchars($v, ENT_XML1, 'UTF-8');
                echo $v;
                echo '</' . $k . '>';
            }
            echo '</node>';
        }
        echo '</nodes>';
    }

}