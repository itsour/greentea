<?php
/**
 * AD連線實作
 */

namespace App\Module\Ldap;

use App\Core\Config\Config;

/**
 * Class Windows AD連線存取
 */
class AD implements LdapConnection
{
    private static $_uniqueInstance;
    private static $_vendor;

    public static function getInstance($vendor = 'main')
    {
        self::$_vendor = $vendor;
        if (null !== static::$_uniqueInstance) {
            return static::$_uniqueInstance;
        }

        try {
            $env = Config::_getEnv('ldap');
            $hostIp = $env[$vendor]['host'];
            $port = $env[$vendor]['port'];

            $ldapConnection = ldap_connect($hostIp, $port) or die("無法連接至AD，請聯絡認證廠商");
            //以下兩行務必加上，否則 Windows AD 無法在不指定 OU 下，作搜尋的動作
            ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldapConnection, LDAP_OPT_REFERRALS, 0);
            
            return static::$_uniqueInstance = $ldapConnection;
        } catch (\Exception $e) {
            echo get_class($e), ': ', $e->getMessage(), "\n";
            exit;
        }
    }

    public static function searchEntries($dn, $filter, $user='', $password='', $domain='', array $attributes=null)
    {
        if (null == static::$_uniqueInstance) {
            throw new \Exception("Please call getInstance before search");
        }

        $ldapbind = self::bindUser($user, $domain, $password);
        if (true == $ldapbind) {
            if ($attributes == null || empty($attributes)) {
                $searchResult = ldap_search(static::$_uniqueInstance, $dn, $filter);
            } else {
                $searchResult = ldap_search(static::$_uniqueInstance, $dn, $filter, $attributes);
            }
            $entries = ldap_get_entries(static::$_uniqueInstance, $searchResult);
        } else {
            throw new \Exception("LDAP bind failed");
        }
        return $entries;
    }

    public static function closeConnection()
    {
        if (null !== static::$_uniqueInstance) {
            ldap_close(static::$_uniqueInstance);
            static::$_uniqueInstance = null;
        }
    }

    private static function bindUser($user='', $domain='', $password='')
    {
        $env = Config::_getEnv('ldap');

        if (null == $user) {
            $user =  $env[self::$_vendor]['user'];
        }
        if (null == $domain) {
            $domain = $env[self::$_vendor]['domain'];
        }
        if (null == $password) {
            $password = $env[self::$_vendor]['pass'];
        }
        $ldaprdn=$user.'@'.$domain;

        $ldapbind = @ldap_bind(static::$_uniqueInstance, $ldaprdn, $password);
        return $ldapbind;
    }
}