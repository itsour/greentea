<?php
/**
 * AD連線實作
 */

namespace App\Module\Ldap;

use App\Core\Config\Config;

/**
 * Class 模擬Windows AD連線存取
 */
class MockAD implements LdapConnection
{
    private static $_uniqueInstance;
    private static $_vendor;

    public static function getInstance($vendor = 'main')
    {
        self::$_vendor = $vendor;
        if (null !== static::$_uniqueInstance) {
            return static::$_uniqueInstance;
        }

        try {
            $env = Config::_getEnv('ldap');
            $hostIp = $env[$vendor]['host'];
            $port = $env[$vendor]['port'];

            $ldapConnection = array('mockad');
            
            return static::$_uniqueInstance = $ldapConnection;
        } catch (\Exception $e) {
            echo get_class($e), ': ', $e->getMessage(), "\n";
            exit;
        }
    }

    public static function searchEntries($dn, $filter, $user='', $password='', $domain='', array $attributes=null)
    {
        if (null == static::$_uniqueInstance) {
            throw new \Exception("Please call getInstance before search");
        }

        $ldapbind = self::bindUser($user, $domain, $password);
        if (true == $ldapbind) {
            if (in_array($filter, array('(sAMAccountName=blueplanet)'))){
                $entries = array(
                    array(
                        'distinguishedname' => array('CN=藍星球,OU=第二科,OU=資訊室,OU=台北總局,DC=net,DC=bsmi,DC=gov,DC=tw;'),
                        'samaccountname' => array('blueplanet'),
                        'name' => array('藍星球'),
                        'title' => array('藍星球Mock Account'),
                        'mail' => array('bluepalnet@blueplanet.com.tw'),
                        'company_name' => array('藍星球Mock Account'),
                        'department_name' => array('藍星球Mock Account')
                    )
                );
            } else {
                $entries = array();
            }

        } else {
            throw new \Exception("LDAP bind failed");
        }
        return $entries;
    }

    public static function closeConnection()
    {
        if (null !== static::$_uniqueInstance) {
            static::$_uniqueInstance = null;
        }
    }

    private static function bindUser($user='', $domain='', $password='')
    {
        $env = Config::_getEnv('ldap');

        if (null == $user) {
            $user =  $env[self::$_vendor]['user'];
        }
        if (null == $domain) {
            $domain = $env[self::$_vendor]['domain'];
        }
        if (null == $password) {
            $password = $env[self::$_vendor]['domain'];
        }
        $ldaprdn=$user.'@'.$domain;
        
        $ldapbind = true;
        return $ldapbind;
    }
}