<?php
/**
 * Interface of Ldap Adapter
 */

namespace App\Module\Ldap;

/**
 * Interface of Ldap Adapter
 */
interface LdapConnection
{
    /**
     * Get ldap link object
     *
     * @param string $vendor
     * @return object ldap link object
     */
    public static function getInstance($vendor);

    /**
     * Search ldap entries
     *
     * @param string $dn Base DN
     * @param string $filter 篩選條件,eg:DC=ccnet,DC=tcc
     * @param string $user 使用者帳號
     * @param string $password 密碼
     * @param string $domain 網域
     * @param array $attributes (option)指定要取得的資料欄位
     * @return array 搜尋結果
     */
    public static function searchEntries($dn, $filter, $user='', $password='', $domain='', array $attributes=null);

    /**
     * 關閉LDAP連結
     */
    public static function closeConnection();
}