<?php

namespace App\Model;

use App\Core\Common\DibiBaseModel;
use App\Core\Common\ApiErrorException\ClientApiErrorException;
use App\Core\Common\ApiErrorException\ServerApiErrorException;
use App\Utility\Utility;
use Dibi;

require_once __DIR__ . "/../../vendor/autoload.php";

class Demo extends DibiBaseModel
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * [pseudo code]
     * db操作有時需要rollback，所以額外try/catch，如不需要rollback，可移除try/catch
     * 僅提供概念，註解部分的細節請自行實作
     */
    public function add($param, $files)
    {
        $param===null ? $param=[] : '';
        // $this->verifyRequest($param, $files);
        try {
            $this->db->begin();
            $insertId = $this->insertData($param);
            // $this->dealWithUploadFiles($file, $insertId);
            $this->db->commit();
        }catch(Dibi\Exception $e) {
            $this->db->rollback();
            throw new ServerApiErrorException($e->getMessage());
        }
    }

    public function list($param)
    {
        return [
            ['sno' => 1, 'title' => 'test1'],
            ['sno' => 2, 'title' => 'test2'],
            ['sno' => 3, 'title' => 'test3'],
        ];
    }

    /**
     * [pseudo code]
     * 一般而言，除了上述rollback情況，直接throw exception即可
     * 僅提供概念，註解部分的細節請自行實作
     */
    public function detail($param)
    {
        $checkResult = Utility::checkRequired($param, ['id']);
        if(!$checkResult['success']) {
            throw new ClientApiErrorException('param中沒有<id>', '不當操作，請選擇id', '<id:%i>欄位為必填');
        }

        $rows = array(
            ['id'=>1, 'title'=>'標題1'],
            ['id'=>2, 'title'=>'標題2'],
            ['id'=>3, 'title'=>'標題3'],
            ['id'=>4, 'title'=>'標題4'],
            ['id'=>5, 'title'=>'標題5'],
        );

        $rowIds = array_column($rows, 'id');
        if(!in_array($param['id'], $rowIds)) {
            throw new ClientApiErrorException('id('.$param['id'].')不存在table('.$this->table.')內', '不當操作，請重新選擇id');
        }
        $allData = array_combine($rowIds, $rows);

        return $allData[$param['id']];
    }

    /**
     * For unit-test, custom client exception error code
     */
    public function customClientErrorCode(int $code)
    {
        throw new ClientApiErrorException('customer error', 'customer error', '', null, $code);
    }

    /**
     * For unit-test, custom server exception error code
     */
    public function customServerErrorCode(int $code)
    {
        throw new ServerApiErrorException('customer error', 'customer error', '', null, $code);
    }
}
