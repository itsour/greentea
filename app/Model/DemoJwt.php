<?php

namespace App\Model;

use App\Core\Common\DibiBaseModel;
use App\Core\Common\ApiErrorException\ClientApiErrorException;
use App\Core\Common\ApiErrorException\ServerApiErrorException;
use App\Utility\Utility;
use App\Module\JwtTool;
use Dibi;

require_once __DIR__ . "/../../vendor/autoload.php";

class DemoJwt
{

    public function __construct()
    {
    }

    public function testWithoutLogin() 
    {
        return 'test success without login';
    }

    public function testWithLogin()
    {
        return 'test success with login';
    }

    public function login($param)
    {
        $checkResult = Utility::checkRequired($param, ['email', 'password']);
        if(!$checkResult['success']) {
            throw new ClientApiErrorException('param中沒有<email> <password>', '不當操作，請輸入email, password');
        }

        if ($param['email'] != 'blueplanet@blueplanet.com.tw' || $param['password'] != '89787198') {
            throw new ClientApiErrorException('錯誤 email 或 password', '錯誤 email 或 password');
        }

        $userInfo = [
            "jti"           => hash('sha256', strtotime("now").'.blueplanet@blueplanet.com.tw'),
            "iss"           => 'saturn_cloud',
            "aud"           => 'blueplanet@blueplanet.com.tw',
            "sub"           => "loginSuccess",
            "exp"           => strtotime("30 seconds"),
            "iat"           => strtotime("now"),
            "name"          => 'JWT demo',
            "status"        => '一方通行',
            "login_time"    => date('Y-m-d H:i:s', time()),
        ];
        $token = JwtTool\JwtTool::encode($userInfo);
        return $token;
    }

    public function logout()
    {
        return 'logout success';
    }
}