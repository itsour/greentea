<?php
namespace App\Model;

use App\Core\Db\Db;
use App\Core\Config\Config;
use App\Module\Office\Excel;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use App\Core\Logger\FileLogger;

require_once __DIR__ . "/../../vendor/autoload.php";

/**
 * The class is for example demo 
 */
class Example
{
    public function getConfig()
    {
        $config = new Config();
        $result = $config('system.web.system_url');
        return $result;
    }
    
    public function connectDB()
    {
        $dibi = new \App\Core\Common\DibiBaseModel();
        $db = $dibi->connectDatabase('main');
        $query = $db->query('SELECT * FROM dbo.question');
        $qr = $query->fetchAll();
        return $qr;
    }

    public function downloadExcel()
    {
        $excel = new Excel();
        $excel->setActiveSheetIndex(0);
        //default outlook
        $excel->getDefaultStyle()->getFont()->setName('微軟正黑體');
        $excel->getDefaultStyle()->getFont()->setSize(12);
        $excel->getDefaultStyle()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $excel->getDefaultStyle()->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $excel->getActiveSheet()->getDefaultColumnDimension()->setWidth(15);


        $excel->getActiveSheet()->setCellValue("A1", "編號");
        // $excel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

        $excel->getActiveSheet()->setCellValue("B1", "處理狀態");
        // $excel->getActiveSheet()->getColumnDimension("B")->setWidth(20);

        $excel->getActiveSheet()->setCellValue("C1", "回報內容");
        $excel->getActiveSheet()->getColumnDimension("C")->setWidth(50);

        $excel->getActiveSheet()->setCellValue("D1", "回報使用者");
        // $excel->getActiveSheet()->getColumnDimension("B")->setWidth(20);

        $excel->getActiveSheet()->setCellValue("E1", "回報時間");
        $excel->getActiveSheet()->setCellValue("F1", "處理內容");
        $excel->getActiveSheet()->getColumnDimension("F")->setWidth(50);
        $excel->getActiveSheet()->setCellValue("G1", "處理者");
        $excel->getActiveSheet()->setCellValue("H1", "處理時間");
        $excel->download();
    }

    public function errorLog($param)
    {
        try {
            $dibi = new \App\Core\Common\DibiBaseModel();
            $db = $dibi->connectDatabase('main');
            $query = $db->query('SELECT * FROM dbo.related_links');
            $qr = $query->fetchAll();
        } catch (\Exception $e) {
            FileLogger::error($e->getMessage(), $param, 'log.txt', __METHOD__);
        }
    }

    public function debugLog($param)
    {
        FileLogger::debug('測試debug message', array(), 'log.txt', __CLASS__);
    }
}