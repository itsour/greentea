<?php

namespace App\Service;

use App\Core\Common\BaseService;

/**
 * Service class for demo and unit-test
 */
class DemoBenchmarkService extends BaseService
{
    /**
     * 壓測使用,透過route進到service層後直接回傳
     *
     * @param mixed $param
     * @return array
     */
    protected function benchmarkRoute($param)
    {
        $this->resultObj->setSuccess();
        return $this->resultObj->apiResult;
    }
}