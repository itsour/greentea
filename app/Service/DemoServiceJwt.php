<?php
namespace App\Service;

use App\Core\Common\BaseService;
use App\Model\DemoJwt;

class DemoServiceJwt extends BaseService
{
    private $demoJwt;

    public function __construct()
    {
        parent::__construct();
        $this->demoJwt = new DemoJwt();
    }

    protected function testWithoutLogin()
    {
        $payload = $this->demoJwt->testWithoutLogin();
        $this->resultObj->setSuccess($payload);
        return $this->resultObj->apiResult;
    }

    protected function testWithLogin()
    {
        $payload = $this->demoJwt->testWithLogin();
        $this->resultObj->setSuccess($payload);
        return $this->resultObj->apiResult;
    }

    protected function login($param)
    {
        $payload = $this->demoJwt->login($param);
        $this->resultObj->setSuccess($payload);
        return $this->resultObj->apiResult;
    }

    protected function logout()
    {
        $payload = $this->demoJwt->logout();
        $this->resultObj->setSuccess($payload);
        return $this->resultObj->apiResult;
    }
}