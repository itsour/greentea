<?php

namespace App\Service;

use App\Core\Common\BaseService;
use App\Model\Demo;

/**
 * Service class for demo and unit-test
 */
class DemoService extends BaseService
{
    private $demo;

    public function __construct()
    {
        parent::__construct();
        $this->demo = new Demo();
    }  

    protected function add($param, $files)
    {
        $result = $this->demo->add($param, $files);
        $this->resultObj->setSuccess($result, '成功新增一筆資料');
        return $this->resultObj->apiResult;
    }

    protected function list($param)
    {
        $payload = $this->demo->list($param);
        $this->resultObj->setSuccess($payload);
        return $this->resultObj->apiResult;
    }

    protected function detail($param)
    {
        $payload = $this->demo->detail($param);
        $this->resultObj->setSuccess($payload);
        return $this->resultObj->apiResult;
    }

    protected function clientFail($code)
    {
        $this->demo->customClientErrorCode($code);
    }

    protected function serverFail($code)
    {
        $this->demo->customServerErrorCode($code);
    }
}