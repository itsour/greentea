<?php
namespace App\Service;

use App\Core\Common\ApiResultImpl;
use App\Core\Common\BaseController;
use App\Model\Demo;
use App\Utility\HttpUtility;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DemoController extends BaseController
{
    private $demo;
    
    public function __construct()
    {
        parent::__construct();
        $this->demo = new Demo();
    }

    protected function add(ServerRequestInterface $request, ResponseInterface $response, array $args)
    {
        $param = $request->getParsedBody();
        $files = $request->getUploadedFiles();
        $payload = $this->demo->add($param, $files);
        $this->resultObj->setSuccess($payload, '成功新增一筆資料');
        return HttpUtility::responseWithJson($response, $this->resultObj->apiResult);
    }

    protected function list(ServerRequestInterface $request, ResponseInterface $response, array $args)
    {
        $param = $request->getParsedBody();
        $payload = $this->demo->list($param);
        $this->resultObj->setSuccess($payload);
        return HttpUtility::responseWithJson($response, $this->resultObj->apiResult);
    }

    protected function detail(ServerRequestInterface $request, ResponseInterface $response, array $args)
    {
        $param = $request->getQueryParams();
        $payload = $this->demo->detail($param);
        $this->resultObj->setSuccess($payload);
        return HttpUtility::responseWithJson($response, $this->resultObj->apiResult);
    }

    // 以下寫法會產生TypeError
    // Return value of Slim\Handlers\Strategies\RequestResponse::__invoke() must implement interface Psr\Http\Message\ResponseInterface, string returned
    // protected function tryError()
    // {
    //     $this->resultObj->setSuccess([]);
    //     return json_encode($this->resultObj->apiResult);
    // }
}