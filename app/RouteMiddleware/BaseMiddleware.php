<?php
namespace App\RouteMiddleware;

use App\Core\Debug\Debug;
use App\Utility\HttpUtility;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Factory\StreamFactory;

class BaseMiddleware
{
    private $bpDebug;

    public function __construct($debug=null)
    {
        if ($debug) {
            $this->bpDebug = $debug;
        }
    }

    public function __invoke(Request $request, RequestHandler $handler)
    {
        if ($this->bpDebug instanceof Debug) {
            $this->bpDebug->beforeRoute();
        }

        $response = $handler->handle($request);

        if ($this->bpDebug instanceof Debug) {
            $debugInfo = $this->bpDebug->afterRoute();
            
            if ($debugInfo != null) {
                $bodyContent = (string)$response->getBody();
                $contentArr = json_decode($bodyContent, true);
                if (json_last_error() === JSON_ERROR_NONE && is_array($contentArr)) {
                    $contentArr['debug']['mark2'] = $debugInfo;
                    $response = HttpUtility::responseWithJson($response, $contentArr);
                } else {
                    //ob_clean(); //清掉原本要輸出的內容,只留debug
                    $factory = new StreamFactory();
                    $bodyStream = $factory->createStream(json_encode($debugInfo, JSON_UNESCAPED_UNICODE));
                    $response = $response->withBody($bodyStream);
                }
            }
        }
    
        return $response;
    }
}