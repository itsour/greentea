<?php
namespace App\RouteMiddleware;

use Psr\Http\Message\ResponseInterface as Response;
use App\Core\Common\ApiResultImpl;
use App\Core\Debug\Debug;
use App\Core\Config\Config;
use App\RouteMiddleware\BaseMiddleware;
use App\RouteMiddleware\CorsMiddleware;
use App\Utility\HttpUtility;
use Slim\App;
use Tuupola\Middleware\JwtAuthentication;

/**
 * 框架的middleware分層  
 * 由於middleware分層較複雜, 故透過此類別處理分層的設定
 */
class Mark2MiddleLayering
{
    private $bpDebug;

    /**
     * middleware分層類別建構函式
     *
     * @param Debug|null $debug 若需要debug功能,需傳入 \App\Core\Debug\Debug 物件
     */
    public function __construct($debug=null)
    {
        if ($debug) {
            $this->bpDebug = $debug;
        }
    }

    /**
     * 設定middleware的分層
     *
     * @param App $app
     */
    public function layerMiddlewares(App &$app)
    {
        $app->add(new BaseMiddleware($this->bpDebug));
        $config = new Config();

        if (true === $config->getConfig('jwt.enable', false)) {
            $jwt = $config->getConfig('jwt.jwt');
            $app->add(new JwtAuthentication([
                "secure"        => $jwt['secure'],
                "path"          => $jwt['path'],
                "ignore"        => $jwt['ignore'],
                "attribute"     => $jwt['attribute'],
                "secret"        => $jwt['secret'],
                "algorithm"     => $jwt['algorithm'],
                "error"         => function ($response, $arguments) {
                    $resultObj = new ApiResultImpl();
                    $resultObj->setFail(400, $arguments["message"], $arguments["message"], $arguments);
                    return HttpUtility::responseWithJson($response, $resultObj->apiResult);
                }
            ]));
        }
        //用來解析HTTP Body的middleware, 需要放在addErrorMiddleware之前
        $app->addBodyParsingMiddleware();
        //理論上要放在最外層，考量到前端本機端開發時會遇到CORS的問題，故往內一層
        $app->addErrorMiddleware($this->bpDebug->getSlimDisplayError(), true, true);
        //處理CORS設設定
        $app->add(new CorsMiddleware($config));
    }
}
