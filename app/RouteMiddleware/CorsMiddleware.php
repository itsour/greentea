<?php
namespace App\RouteMiddleware;

use App\Core\Config\Config;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

class CorsMiddleware
{
    private $config;
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function __invoke(Request $request, RequestHandler $handler)
    {
        $response = $handler->handle($request);

        $securityCsp = "default-src 'self' www.google-analytics.com;"
        . " script-src 'self' 'nonce-nHEyhxlaUGSQRDBmOZJ3';"
        . " img-src 'self' www.facebook.com cdnjs.cloudflare.com  data:;"
        . " style-src 'self' fonts.googleapis.com cdnjs.cloudflare.com use.fontawesome.com;"
        . " font-src 'self' fonts.gstatic.com use.fontawesome.com;"
        . " base-uri 'self';"
        . " form-action 'self';"
        . " frame-ancestors 'self'";
        $defaultAllowOrigin = '*';
        $allowOrigin = $this->config->getConfig('system.web.access_control_allow_origin', $defaultAllowOrigin);
        $allowOrigin = ($allowOrigin == null) ? $defaultAllowOrigin : $allowOrigin;

        return $response
            ->withHeader('X-Frame-Options','SAMEORIGIN') /*解決X-Frame-Options Header Not Set*/
            ->withHeader('X-Content-Type-Options','nosniff') /* 解決X-Content-Type-Options Header Missing*/
            ->withHeader('X-XSS-Protection','1; mode=block') /*解決Web Browser XSS Protection Not Enabled*/
            ->withHeader('Content-Security-Policy', $securityCsp) /* 解決後端的Content Security Policy (CSP) Header Not Set */
            ->withHeader('Access-Control-Allow-Origin', $allowOrigin)
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    }
}