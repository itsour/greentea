<?php
namespace App\Core\Debug;

use App\Core\Config\Config;
use App\Utility\Utility;

class Debug
{
    private $isTimerOn = false;
    private $timer;
    private $debugLevel;
    private $debugKey = 'bp_debug';
    private $inputSecretKey;
    private $validSecretKey;
    
    //private $_slimSetting;
    private $_isSlimDisplayError;
    private $_requestMethod;
    private $_requestGetVar;
    private $_requestPostVar;
    private $_serverInfo;
    private $_sessionInfo;
    private $_clientAgent;
    private $_clientIp;
    private $_accessUrl;
    private $_accessFrom;
    private $_phpVersion;
    // private $_phpInfo;
    private $_currentEnv;
    private $_appVersion; //版本號
    private $_filesInfo;
    private $_cookieInfo;
    private $_phpLoadedExtension;
    private $_frameworkVersion;

    public function __construct($debugLevel=null)
    {
        $this->init($debugLevel);
    }

    public function init($debugLevel)
    {
        $this->assignRequestMethod();
        $this->assignDebugLevel($debugLevel);
        $this->initByDebugLevel();
    }

    public function beforeRoute()
    {
        if ($this->debugLevel === 5) {
            $this->clickTimer();
        }
    }

    public function afterRoute()
    {
        $debugInfo = '';
        $usageSeconds = 0;
        if ($this->debugLevel === 5) {
            $usageSeconds = $this->clickTimer();
            //整理資料回傳
            $debugInfo = $this->getDebugInfo($usageSeconds);
        }
        return $debugInfo;
    }

    public function destory()
    {
        // Currently no destory required
    }

    private function assignInputSecretKey()
    {
        if ($this->_requestMethod == 'GET') {
            $this->inputSecretKey = $_GET[$this->debugKey] ?? '';
        } elseif ($this->_requestMethod == 'POST') {
            $this->inputSecretKey = $_POST[$this->debugKey] ?? '';
        }
    }

    private function assignDebugLevel($debugLevel)
    {
        if ($debugLevel) {
            $this->debugLevel = $debugLevel;
        } else {
            $this->debugLevel = 1;
            $this->assignInputSecretKey();
            $this->assignValidSecretKey();
            if ($this->inputSecretKey != null && $this->inputSecretKey == $this->validSecretKey) {
                $this->debugLevel = 5;
            }
        }
    }

    private function assignValidSecretKey()
    {
        $env = Config::getEnvAll();
        $this->validSecretKey = $env['SECRET_KEY'] ?? '';
    }

    private function initByDebugLevel()
    {
        if ($this->debugLevel === 1) {
            $this->_isSlimDisplayError = $this->getConfigSlimDisplayError();
            $this->assignRequestGetVar();
            $this->assignRequestPostVar();
            $this->assignClientAgent();
            $this->assignClientIp();
            $this->assignAccessUrl();
            $this->assignSessionInfo();
            $this->assignAccessFrom();
        } elseif ($this->debugLevel === 5) {
            error_reporting(E_ALL);
            $this->_isSlimDisplayError = true;
            $this->isTimerOn = false;
            $this->assignRequestGetVar();
            $this->assignRequestPostVar();
            $this->assignClientAgent();
            $this->assignClientIp();
            $this->assignAccessUrl();
            $this->assignSessionInfo();
            $this->assignAccessFrom();
            //extra
            $this->assignPhpVersion();
            $this->assignServerInfo();
            $this->assignCookieInfo();
            $this->assignCurrentEnv();
            $this->assignFileInfo();
            $this->assignPhpLoadedExtension();
            $this->assignFrameworkVersion();
        } else {

        }
    }

    private function assignRequestMethod()
    {
        $this->_requestMethod = $_SERVER['REQUEST_METHOD'] ?? '';
    }

    private function assignRequestGetVar()
    {
        $this->_requestGetVar = $_GET;
    }

    private function assignRequestPostVar()
    {
        $this->_requestPostVar = $_POST;
    }

    private function assignServerInfo()
    {
        $this->_serverInfo = $_SERVER;
    }

    private function assignSessionInfo()
    {
        $this->_sessionInfo = empty($_SESSION) ? [] : $_SESSION;
    }

    private function assignClientAgent()
    {
        $this->_clientAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }

    private function assignClientIp()
    {
        $this->_clientIp = Utility::get_client_ip();
    }

    private function assignAccessUrl()
    {
        $this->_accessUrl = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] : '';
    }

    private function assignAccessFrom()
    {
        $this->_accessFrom = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
    }

    private function assignPhpVersion()
    {
        $this->_phpVersion = phpversion();
    }

    private function getConfigSlimDisplayError()
    {
        $config = new Config();
        return $config('system.slim_display_error');
    }

    private function assignCurrentEnv()
    {
        $this->_currentEnv = Config::getAppEnv();
    }

    private function assignFileInfo()
    {
        $this->_filesInfo = empty($_FILES) ? [] : $_FILES;
    }

    private function assignCookieInfo()
    {
        $this->_cookieInfo = empty($_COOKIE) ? [] : $_COOKIE;
    }

    private function assignPhpLoadedExtension()
    {
        $this->_phpLoadedExtension = get_loaded_extensions();
    }

    private function assignFrameworkVersion()
    {
        $this->_frameworkVersion = Config::getBpFrameworkVersion();
    }

    private function clickTimer()
    {
        if ($this->isTimerOn === true) {
            $seconds = microtime(true) - $this->timer;
            $this->isTimerOn = false;
        } else {
            $seconds = 0;
            $this->isTimerOn = true;
            $this->timer = microtime(true);
        }
        return $seconds;
    }

    private function getDebugInfo($usageSeconds)
    {
        return [
            'isSlimDisplayError' => $this->_isSlimDisplayError,
            'accessUrl' => $this->_accessUrl,
            'requestMethod' => $this->_requestMethod,
            'requestGetVar' => $this->_requestGetVar,
            'requestPostVar' => $this->_requestPostVar,
            'phpVersion' => $this->_phpVersion,
            'phpLoadedExtension' => $this->_phpLoadedExtension,
            'serverInfo' => $this->_serverInfo,
            'sessionInfo' => $this->_sessionInfo,
            'fileInfo' => $this->_filesInfo,
            'clientIp' => $this->_clientIp,
            'clientAgent' => $this->_clientAgent,
            'accessFrom' => $this->_accessFrom,
            'currentEnv' => $this->_currentEnv,
            'frameworkVersion' => $this->_frameworkVersion,
            'usageSeconds' => $usageSeconds
        ];
    }

    public function getSlimDisplayError()
    {
        return $this->_isSlimDisplayError;
    }
}