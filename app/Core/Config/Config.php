<?php
/**
 * 實作對config json檔的存取
 */
namespace App\Core\Config;

use App\Core\Common\EnvAbstract;
use App\Core\Common\ApiResultImpl;

/**
 * Class Config, 用來存取config json檔的設定值
 */
class Config extends EnvAbstract
{
    protected $configName;

    /**
     * Get target config value
     * @param string $configName Config name, eg. system.web.system_url
     * @param mixed $defaultValue Return default value if config value not found; 
     * @return array|string  $allConfig|$config
     * @throws \UnexpectedValueException If defaultValue is null and cannot get config value, this exception will be throw
     */
    public function __invoke($configName='', $defaultValue=null) 
    {
        return $this->getConfig($configName, $defaultValue);
    }

    /**
     * Get target config value
     * @param string $configName Config name, eg. system.web.system_url
     * @param mixed $defaultValue Return default value if config value not found; 
     * @return array|string  $allConfig|$config
     * @throws \UnexpectedValueException If defaultValue is null and cannot get config value, this exception will be throw
     */
    public function getConfig(string $configName, $defaultValue=null)
    {
        $arrParameter = explode('.', $configName);
        $this->configName = $arrParameter[0];
        $allConfig = self::_getEnv($this->configName, $defaultValue);
        unset($arrParameter[0]);

        if (count($arrParameter) === 0) {
            return $allConfig;
        }

        if ($defaultValue === null) {
            return $this->getSingleConfigValue($arrParameter, $allConfig);
        } else {
            return $this->getSingleConfigValueOrDefault($arrParameter, $allConfig, $defaultValue);
        }
    }

    /**
     * Drill down to get last config value from given parameter
     */
    public function getSingleConfigValue(array $arrParameter, $allConfig)
    {
        foreach ($arrParameter as $param) {
            if (!isset($allConfig[$param])) {
                throw new \UnexpectedValueException('Config "'. $param . '" not set.');
            }
            $allConfig = $allConfig[$param];
        }

        return $allConfig;
    }

    public function getSingleConfigValueOrDefault(array $arrParameter, $allConfig, $defaultValue)
    {
        foreach ($arrParameter as $param) {
            if (!isset($allConfig[$param])) {
                return $defaultValue;
            }
            $allConfig = $allConfig[$param];
        }

        return $allConfig;
    }

    /**
     * 取得目前系統的環境(dev/testing/staging/production)
     *
     * @return string 目前系統環境名稱
     */
    public function getAppEnvForApiResult()
    {
        $appEnv = self::getAppEnv();
        $apiObj = new ApiResultImpl();
        $apiResult = $apiObj->init();
        $payload = ['env' => $appEnv];
        $apiResult = $apiObj->setSuccess($payload);
        return $apiResult;
    }
}