<?php
/**
 * API Result 實作
 */
namespace App\Core\Common;

use App\Core\Config\Config;

/**
 * Class ApiResultImpl: API Result實作
 */
class ApiResultImpl implements ApiResultInterface
{
    public $apiResult;
    private $invalidKey = ['success', 'code', 'payload', 's_message', 'e_message', 'f_message', 'debug', ''];
    const CODE_INIT = 100;
    const CODE_SUCCESS = 200;
    const CODE_CLIENT_FAIL = 400;
    const CODE_SERVER_FAIL = 500;

    public function __construct()
    {
        $this->apiResult = array();
    }

    /**
     * 初值化API回傳結構
     * code=100
     */
    public function init()
    {
        $this->setApiResult(false, self::CODE_INIT, null, '');
        $this->unsetPayload();
        return $this->apiResult;
    }

    /**
     * 設定API回傳狀態為成功
     * code=200
     */    
    public function setSuccess($payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null)
    {
        $this->setApiResult(true, self::CODE_SUCCESS, $payload, $sMessage, $eMessage, $fMessage, $debug);
        return $this->apiResult;
    }

    /**
     * 設定API回傳狀態為錯誤
     * 預設code=400/500
     */
    public function setFail(int $code, $payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null)
    {
        $this->setApiResult(false, $code, $payload, $sMessage, $eMessage, $fMessage, $debug);
        return $this->apiResult;
    }

    /**
     * 用指定的Key值來設定apiResult的payload中的值
     */
    public function setPayloadByKey($key, $value)
    {
        if (!isset($this->apiResult['payload']) || !is_array($this->apiResult['payload'])) {
            $this->apiResult['payload'] = array();
        }
        $this->apiResult['payload'][$key] = $value;
        return $this->apiResult;
    }

    /**
     * 設定API回傳狀態為錯誤
     * Client side error (預設code=400)
     */
    public function setClientFail($payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null)
    {
        $this->setApiResult(false, self::CODE_CLIENT_FAIL, $payload, $sMessage, $eMessage, $fMessage, $debug);
        return $this->apiResult;
    }

    /**
     * 設定API回傳狀態為錯誤
     * Server side error (預設code=500)
     */
    public function setServerFail($payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null)
    {
        $this->setApiResult(false, self::CODE_SERVER_FAIL, $payload, $sMessage, $eMessage, $fMessage, $debug);
        return $this->apiResult;
    }

    /**
     * 設定apiResult額外的key-value值
     * 不允許key值為'success', 'code', 'payload', 's_message', 'e_message', 'f_message', 'debug', ''
     *
     * @param string $key
     * @param mixed $value
     * @return array apiResult
     */
    public function setExtraApiResult(string $key, $value)
    {
        if (false === $this->checkExtraKeyValid($key)) {
            throw new \InvalidArgumentException('Invalid key "'. $key .'"');
        }

        $this->apiResult[$key] = $value;
        return $this->apiResult;
    }

    /**
     * 清除apiResult額外的key值
     *
     * @param string $key
     * @return void
     */
    public function unsetExtraApiResult(string $key)
    {
        if (false === $this->checkExtraKeyValid($key)) {
            throw new \InvalidArgumentException('Invalid key "'. $key .'"');
        }

        unset($this->apiResult[$key]);
    }

    private function checkExtraKeyValid(string $key)
    {
        $valid = true;
        if (in_array($key, $this->invalidKey, true)) {
            $valid = false;
        }
        return $valid;
    }

    public function setApiResult(bool $isSuccess, int $code, $payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null)
    {
        $this->apiResult['success'] = $isSuccess;
        $this->apiResult['code'] = $code;
        $this->apiResult['s_message'] = $sMessage ?? '';
        $env = Config::getEnvAll();
        if(isset($env['API_RESULT_ERROR_MESSAGE']) && $env['API_RESULT_ERROR_MESSAGE']===true) {
            ($code!==200) ? ($this->apiResult['e_message']=$eMessage) : '';
        }
        (!empty($fMessage)) ? ($this->apiResult['f_message']=$fMessage) : '';
        if ($payload !== null) {
            $this->setPayload($payload);
        } else {
            $this->unsetPayload();
        }
        
        $this->setDebug($debug);
    }

    private function setPayload($payload)
    {
        if (isset($payload)) {
            $this->apiResult['payload'] = $payload;
        }
    }

    private function unsetPayload()
    {
        unset($this->apiResult['payload']);
    }

    private function setDebug($debug)
    {
        if (isset($debug) && $debug !== '') {
            if (is_array($debug)) {
                $this->apiResult['debug'] = $debug;
            } else {
                $this->apiResult['debug'][] = $debug;
            }
        }
    }
}