<?php

namespace App\Core\Common\ApiErrorException;

use App\Core\Common\BaseException;

/**
 * Server-side error (code: 500)
 */
class ServerApiErrorException extends BaseException
{
    /**
     * @param string|null $errorMessage: error message之意，供後端debug用，為exception錯誤內容，當前端使用API有不解的錯誤，可以此給後端trace
     * @param string|null $showMessage: show message之意，供前端使用，為 html文字，可直接呈現的訊息
     * @param string|null $formatMessage: format message之意，供前端轉譯使用
     * @param array|null $payload: 回傳內容,若給null會將payload清掉(即unset)
     * @param int $code: 錯誤代碼,預設為500
     */
    public function __construct($errorMessage, $showMessage=null, $formatMessage=null, $payload=null, $code=500) {
        parent::__construct($errorMessage, $showMessage, $formatMessage, $payload, $code);
    }
}