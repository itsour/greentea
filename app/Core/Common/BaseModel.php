<?php
/**
 * Framework Mark2底層
 */
namespace App\Core\Common;

use App\Core\Db\DbConnection;

/**
 * Abstract Class BaseModel, 透過DbConnection介面進行資料庫連線,實作ApiResult抽象層
 */
abstract class BaseModel
{
    protected $apiResult;
    protected $db;
    private $resultObj;
    private $dbConn;

    public function __construct(DbConnection $dbConnection)
    {
        $this->resultObj = new ApiResultImpl();
        $this->apiResult = $this->resultObj->apiResult;
        $this->dbConn = $dbConnection;
    }

    /**
     * 連線資料庫
     * @param string $vendor database.json設定檔中vendor的key,預設應為main
     * @return mixed 資料庫連線物件
     */
    public function connectDatabase($vendor)
    {
        $this->db = $this->dbConn::getInstance($vendor);
        return $this->db;
    }

    /**
     * 初值化API回傳結構
     * code=100
     */
    public function init()
    {
        $this->resultObj->init();
        $this->apiResult = $this->resultObj->apiResult;
    }
 
    /**
     * 設定API回傳狀態為成功
     * code=200
     */
    public function setSuccess($payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null)
    {
        $this->resultObj->setSuccess($payload, $sMessage, $eMessage, $fMessage, $debug);
        $this->apiResult = $this->resultObj->apiResult;
    }

    /**
     * 設定API回傳狀態為client端錯誤,如:輸入資料驗證錯誤
     * code=400
     */
    public function setFail(int $code, $payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null)
    {
        $this->resultObj->setFail($code, $payload, $sMessage, $eMessage, $fMessage, $debug);
        $this->apiResult = $this->resultObj->apiResult;
    }


    /**
     * 設定apiResult額外的key-value值
     * 不允許key值為'success', 'code', 'message', 'payload', ''
     *
     * @param string $key
     * @param mixed $value
     */
    public function setExtraApiResult(string $key, $value)
    {
        $this->resultObj->setExtraApiResult($key, $value);
        $this->apiResult = $this->resultObj->apiResult;
    }

    /**
     * 清除apiResult額外的key值
     *
     * @param string $key
     */
    public function unsetExtraApiResult(string $key)
    {
        $this->resultObj->unsetExtraApiResult($key);
        $this->apiResult = $this->resultObj->apiResult;
    }

    /**
     * 將給的key與value置入payload中
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function setPayloadByKey($key, $value)
    {
        $this->resultObj->setPayloadByKey($key, $value);
        $this->apiResult = $this->resultObj->apiResult;
    }

    /**
     * 取得apiResult的值
     *
     * @return array
     */
    public function getApiResult()
    {
        return $this->apiResult;
    }

    /**
     * 取得當下所繼承的子物件class名稱
     *
     * @param object $object 物件
     * @return string 物件class名稱(不含namespace)
     */
    public static function getSubClassName($object)
    {
        return substr(strrchr(get_class($object), '\\'), 1);
    }

    /**
     * 將巴斯卡命名(PascalCase)轉換成小寫底線(snake_case)命名;eg: BluePlanetTable => blue_planet_table
     *
     * @param string $name 巴斯卡命名
     * @return string 小寫底線命名
     */
    public static function convertPascalCaseToSnakeCase($name)
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $name)), '_');
    }
}