<?php

namespace App\Core\Common;

/**
 * Base Exception Interface
 */
class BaseException extends \Exception
{
    private $showMessage;
    private $formatMessage;
    private $payload;

    // Redefine the exception so message isn't optional
    /**
     * Client/Server Error (400/500)
     * @param string|null $errorMessage: error message之意，供後端debug用，為exception錯誤內容，當前端使用API有不解的錯誤，可以此給後端trace
     * @param string|null $showMessage: show message之意，供前端使用，為 html文字，可直接呈現的訊息
     * @param string|null $formatMessage: format message之意，供前端轉譯使用
     * @param array|null $payload: 回傳內容,若給null會將payload清掉(即unset)
     * @param int $code: 結果代碼 (目前exception中定義了400/500)
     * @param \Exception $previous
     */
    public function __construct($errorMessage, $showMessage, $formatMessage, $payload, $code, \Exception $previous = null) {
        // some code
        $this->showMessage = $showMessage;
        $this->formatMessage = $formatMessage;
        $this->payload = $payload;
    
        // make sure everything is assigned properly
        parent::__construct($errorMessage, $code, $previous);
    }

    public function getShowMessage() {
        return $this->showMessage;
    }
    
    public function getFormatMessage() {
        return $this->formatMessage;
    }
    
    public function getPayload() {
        return $this->payload;
    }

    public function __toString() {
        return sprintf('%s: Message: %s; File: %s; Line: %d.', get_class($this), $this->getMessage(), $this->getFile(), $this->getLine());
        // return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}