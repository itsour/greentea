<?php

namespace App\Core\Common;

use App\Utility\HttpUtility;

/**
 * Class BaseController: Implement __call method to support apiResult in cotroller
 */
class BaseController
{
    protected $resultObj;

    public function __construct()
    {
        $this->resultObj = new ApiResultImpl();
    }

    public function __call($method, $arguments)
    {
        if(!method_exists($this, $method)) {
            throw new \Exception($method . ' not fould in Class' . get_class($this));
        }
        try {
            return call_user_func_array(array($this, $method), $arguments);
        }catch (BaseException $e) {
            $this->resultObj->setFail($e->getCode(), $e->getPayload(), $e->getShowMessage(), (string)$e, $e->getFormatMessage());
        }catch (\RuntimeException $e) {
            $this->resultObj->setServerFail(null, '', $e->getMessage());
        }catch (\Exception $e) {
            $this->resultObj->setServerFail(null, '', $e->getMessage());
        }

        // 第2個參數(即$arguments[1])的型別必須是Psr\Http\Message\ResponseInterface
        return HttpUtility::responseWithJson($arguments[1], $this->resultObj->apiResult);
    }
}