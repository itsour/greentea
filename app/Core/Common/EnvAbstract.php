<?php 
/**
 * 環境變數與config設定檔抽象層
 */
namespace App\Core\Common;

/**
 * Abstract Class EnvAbstract
 */
abstract class EnvAbstract
{
    private static $cachedEnv;
    private static $cachedConfig;

    /**
     * Get config settings by name and environment
     * @param string $configName config file name without .json extension
     * @param mixed $defaultValue If not config not exist, return default value
     * @return mixed array of config settings or default value
     * @throws \RuntimeException If config file not exist and no config 
     */
    public static function _getEnv($configName = 'database', $defaultValue=null)
    {
        $defaultAppEnv = 'dev';
        $appEnv = self::getAppEnv();
        $appEnv = ($appEnv == null) ? $defaultAppEnv : $appEnv;
        // Get from self cache if configName exist for improve performance
        if (isset(self::$cachedConfig[$configName])) {
            return self::$cachedConfig[$configName] ?? $defaultValue;
        }

        $configPath = __DIR__.'/../../../config/'.$configName.'.json';
        if (false === file_exists($configPath)) {
            if ($defaultValue !== null) {
                return $defaultValue;
            } else {
                throw new \RuntimeException('Config file of "' . $configName . '" dose not exist');
            }
        }
        $strJson = file_get_contents($configPath);
        $currConfig = json_decode($strJson, true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new \RuntimeException('Unable to parse config "' . $configName . '", json format may be incorrect, error code:' . json_last_error());
        }
        self::$cachedConfig[$configName] = $currConfig[$appEnv];
        return self::$cachedConfig[$configName] ?? $defaultValue;
    }

    /**
     * 取得目前.env檔所設定的環境(APP_ENV)值
     *
     * @return string 目前設定的APP_ENV環境
     */
    public static function getAppEnv()
    {
        $envSetting = self::getEnvAll();
        return $envSetting['APP_ENV'] ?? '';
    }

    /**
     * 取得目前.env檔所設定的所有值
     *
     * @return array 所有設定值的key-value array格式
     */
    public static function getEnvAll()
    {
        if (self::$cachedEnv) {
            return self::$cachedEnv;
        }
        if (false === file_exists(__DIR__.'/../../../.env')) {
            exit('Please create your .env file first');
        }
        self::$cachedEnv = parse_ini_file(__DIR__.'/../../../.env', true, INI_SCANNER_TYPED);
        return self::$cachedEnv;
    }

    /**
     * 取得目前框架版本
     *
     * @return string 框架版本號
     */
    public static function getBpFrameworkVersion()
    {
        $composerFile = __DIR__.'/../../../composer.json';
        if (false === file_exists($composerFile)) {
            return 'Unknown';
        }

        $content = file_get_contents($composerFile);
        $content = json_decode($content, true);
        return $content['version'] ?? 'Unknown';
    }
}
