<?php
/**
 * 實作資料庫存取機制給table class繼承
 */
namespace App\Core\Common;

use App\Core\Db\Db;
use App\Core\Config\Config;
use App\Core\Db\DbOperateInterface;
use App\Utility\Utility;
use App\Core\Logger\FileLogger;
use Dibi;

/**
 * Class DibiBaseModel: Implement database connect and simple db access method using dibi liberary.
 */
class DibiBaseModel extends BaseModel implements SimpleDB, DbOperateInterface
{
    protected $table;
    private $isNeedDBLog;
    private $constructTime = 0;
    private $dbLogTable;

    public function __construct($vendor='main', $needDbLog=null)
    {
        parent::__construct(new Db());
        try {
            $this->connectDatabase($vendor);
        } catch (\Exception $e) {
            $errMsg = get_class($e) . ':' . $e->getMessage();
            FileLogger::alert($errMsg, [], '', __METHOD__);
            throw $e;
        } 
        
        $subModelClassName = self::getSubClassName($this);
        $this->table = self::convertPascalCaseToSnakeCase($subModelClassName);
        $config = new Config();
        //isNeedDBLog優先順序: debug機制(尚未實作) > 繼承class決定 > config檔設定
        $this->isNeedDBLog = $config('system.system_log.is_need_db_log', false);
        if ($needDbLog === true || $needDbLog === false) {
            $this->isNeedDBLog = $needDbLog;
        }

        $this->constructTime = microtime(true);
        $this->dbLogTable = 'log_system';
    }

    public function __destruct()
    {
        $classUsageSeconds = microtime(true) - $this->constructTime;
        if (is_object($this->db)) {
            if (true === $this->isNeedDBLog) {
                $this->addDbSystemLog('', '', $classUsageSeconds);
            }
        }
    }

    /**
     * 取得目前設定的tableName
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * 設定tableName方便SimpleDB存取
     */
    public function setTable(String $tableName)
    {
        $this->table = $tableName;
    }

    /**
     * 開啟DB Log
     */
    public function setDbLogOn()
    {
        $this->isNeedDBLog = true;
    }

    /**
     * 關閉DB Log
     */
    public function setDbLogOff()
    {
        $this->isNeedDBLog = false;
    }

    /**
     * 指定欄位名稱(keyName)和欄位值(keyValue)為條件,取得table單一筆資料
     * @param string $keyName 欄位名稱
     * @param mixed $keyValue 欄位值
     * @return \Dibi\Row|null
     */
    public function getDataByKey($keyName, $keyValue)
    {
        $sql = "SELECT * FROM `$this->table` WHERE %and";
        $whereCond[0] = array("$keyName = ?", $keyValue);
        $qr = $this->db->query($sql, $whereCond);
        return $qr->fetch();
    }

    /**
     * 以多組鍵-值對的assoc array為條件,取得table的單一筆資料
     * 相當於每組鍵-值都是一個where條件,並用AND組合查詢
     * 
     * @param array $keyValuePairs 用來篩選資料的assoc array, key為篩選欄位名, value為篩選值; eg. ['account' => 'blueplanet', 'status' => 1]
     * @return \Dibi\Row|null 單一筆資料,若無資料則為null
     * @throws \UnexpectedValueException 若$keyValuePairs為空時丟出例外錯誤
     */
    public function getDataByMultiKeys(array $keyValuePairs)
    {
        if (empty($keyValuePairs)) {
            throw new \UnexpectedValueException('keyValuePairs is empty');
        }
        $sql = "SELECT * FROM `$this->table` WHERE %and";
        return $this->db->fetch($sql, $keyValuePairs);
    }

    /**
     * 指定欄位名稱(keyName)和欄位值(keyValue)為條件,取得table多筆資料
     * @param string $keyName 欄位名稱
     * @param mixed $keyValue 欄位值
     * @return array 陣列中的每筆資料為Dibi\Row物件,無資料時為空陣列
     */
    public function getRowsByKey($keyName, $keyValue)
    {
        $sql = "SELECT * FROM `$this->table` WHERE %and";
        $whereCond[0] = array("$keyName = ?", $keyValue);
        return $this->db->fetchAll($sql, $whereCond);
    }

    /**
     * 以多組鍵-值對的assoc array為條件,取得table的多筆資料
     * 相當於每組鍵-值都是一個where條件,並用AND組合查詢
     * 
     * @param array $keyValuePairs 用來篩選資料的assoc array, key為篩選欄位名, value為篩選值; eg. ['account' => 'blueplanet', 'status' => 1]
     * @return array 陣列中的每筆資料為Dibi\Row物件,無資料時為空陣列
     * @throws \UnexpectedValueException 若$keyValuePairs為空時丟出例外錯誤
     */
    public function getRowsByMultiKeys(array $keyValuePairs)
    {
        if (empty($keyValuePairs)) {
            throw new \UnexpectedValueException('keyValuePairs is empty');
        }
        $sql = "SELECT * FROM `$this->table` WHERE %and";
        return $this->db->fetchAll($sql, $keyValuePairs);
    }

    /**
     * 於table中新增資料, 若id欄位是自行輸入且非int則會回傳-1而非id值
     * @param array $data 要新增的資料,結構為key-value陣列, eg.['sid'=>'a0001', 'name'=>'blueplanet', 'created_time'=>'2018-07-05']
     * @return int 新增資料的id
     */
    public function insertData(Array $data)
    {
        $sql = "INSERT INTO `$this->table`";
        $this->db->query($sql, $data);
        try {
            //若id欄位是使用者自行輸入且非int, dibi的getInsertId會轉型失敗error，故try catch避免之並回傳-1
            $id = $this->db->getInsertId();
        } catch (Dibi\Exception $e) {
            $id = -1;
        }
        
        return $id;
    }

    /**
     * 依key-value的條件修改table資料
     * @param array $data 要修改的資料,結構為key-value陣列, eg.['name'=>'blueplanet', 'updated_time'=>'2018-07-05']
     * @param string $keyName 欄位名稱
     * @param mixed $keyValue 欄位值
     */
    public function updateDataByKey(Array $data, $keyName, $keyValue)
    {
        $whereCond[0] = array("$keyName = ?", $keyValue);
        $this->db->query("UPDATE `$this->table` SET ", $data, " WHERE %and", $whereCond);
    }

    /**
     * 依key-value的條件刪除table資料
     * @param string $keyName 欄位名稱
     * @param mixed $keyValue 欄位值
     */
    public function deleteDataByKey($keyName, $keyValue)
    {
        $whereCond[0] = array("$keyName = ?", $keyValue);
        $this->db->query("DELETE FROM `$this->table` WHERE %and", $whereCond);
    }

    /**
     * 依key-value的條件查找table資料,若無資料則新增data,若有則更新
     * @param array $data 要新增/修改的資料,結構為key-value陣列, eg.['name'=>'blueplanet', 'updated_time'=>'2018-07-05']
     * @param string $keyName 欄位名稱
     * @param mixed $keyValue 欄位值
     */
    public function upsertDataByKey(Array $data, $keyName, $keyValue)
    {
        $insertId = '';
        $current = $this->getDataByKey($keyName, $keyValue);
        if (empty($current)) {
            $insertId = $this->insertData($data);
        } else {
            $this->updateDataByKey($data, $keyName, $keyValue);
        }
        return $insertId;
    }

    /**
     * 以多組鍵-值對的assoc array為條件,更新table的資料
     *
     * @param array $data 要修改的資料,結構為key-value陣列, eg.['name'=>'blueplanet', 'updated_time'=>'2018-07-05']
     * @param array $keyValuePairs 用來篩選資料的assoc array, key為篩選欄位名, value為篩選值; eg. ['account' => 'blueplanet', 'status' => 1]
     * @throws \UnexpectedValueException 若$keyValuePairs為空時丟出例外錯誤
     */
    public function updateDataByMultiKeys(array $data, array $keyValuePairs)
    {
        if (empty($keyValuePairs)) {
            throw new \UnexpectedValueException('keyValuePairs is empty');
        }
        $this->db->query("UPDATE `$this->table` SET", $data, 'WHERE %and', $keyValuePairs);
    }

    /**
     * 以多組鍵-值對的assoc array為條件,刪除table的資料
     *
     * @param array $keyValuePairs 用來篩選資料的assoc array, key為篩選欄位名, value為篩選值; eg. ['account' => 'blueplanet', 'status' => 1]
     * @throws \UnexpectedValueException 若$keyValuePairs為空時丟出例外錯誤
     */
    public function deleteDataByMultiKeys(array $keyValuePairs)
    {
        if (empty($keyValuePairs)) {
            throw new \UnexpectedValueException('keyValuePairs is empty');
        }
        $this->db->query("DELETE FROM `$this->table` WHERE %and", $keyValuePairs);
    }

    /**
     * @param string $functionName function名稱
     * @param $param 傳入的變數
     * @param $classUsageMicroTime 類別的使用時間(=解構時間微秒-建構時間微秒)
     */
    protected function addDbSystemLog($functionName='', $param='', $classUsageSeconds=null)
    {
        try {
            $dt = Utility::getDateTimeWithMicrosecond();
            $accClass = get_class($this);
            $accIp = Utility::get_client_ip();
            $accMethod = $functionName ?? '';
            $accUrl = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] : 'command line';
            $accFrom = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
            $request = empty($param) ? '' : json_encode($param, JSON_UNESCAPED_UNICODE);
            $result = json_encode($this->apiResult);
            $agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
            $session  = empty($_SESSION) ? '' : json_encode($_SESSION, JSON_UNESCAPED_UNICODE);

            $logData = array(
                'created_time' => $dt,
                'acc_ip' => $accIp,
                'acc_url' => $accUrl,
                'acc_class' => $accClass,
                'acc_method' => $accMethod,
                'session' => $session,
                'request' => $request,
                'result' => $result,
                'acc_from' => $accFrom,
                'agent' => $agent
            );

            if ($classUsageSeconds != null && is_numeric($classUsageSeconds)) {
                $logData['class_usage_seconds'] = $classUsageSeconds;
            }

            $this->db->query("INSERT INTO `$this->dbLogTable`", $logData);
        } catch (\Exception $e) {
            FileLogger::error($e->getMessage(), $logData, '', __METHOD__);
        }
        $this->isNeedDBLog = false;
    }

    public function cleanupLogSystem()
    {
        $config = new Config();
        $keepDays = $config('system.system_log.db_log_keep_days');
        $intervalStr = 'P15D';
        if ($keepDays != null) {
            $intervalStr = 'P' . $keepDays . 'D';
        }

        $date = new \DateTime();
        $date->sub(new \DateInterval($intervalStr));
        $date_string = $date->format('Y-m-d H:i:s.u');
        $this->db->query("DELETE FROM `$this->dbLogTable` WHERE created_time < '".$date_string."'");
    }

    public function query(...$args)
    {
        return $this->db->query($args);
    }

    public function fetch(...$args)
    {
        return $this->db->fetch($args);
    }

    public function fetchAll(...$args): array
    {
        return $this->db->fetchAll($args);
    }

    public function fetchSingle(...$args)
    {
        return $this->db->fetchSingle($args);
    }

    public function fetchPairs(...$args): array
    {
        return $this->db->fetchPairs($args);
    }

    public function fetchAssoc($assoc, ...$args): array
    {
        $qr = $this->db->query($args);
        return $qr->fetchAssoc($assoc);
    }

    /**
     * 交易開始
     * Begins a transaction (if supported).
     *
     * @param string|null $savepoint
     * @return void
     */
    public function begin($savepoint = null)
    {
        return $this->db->begin($savepoint);
    }

    /**
     * 簽入交易
     * Commits statements in a transaction.
     *
     * @param string|null $savepoint 
     * @return void
     */
    public function commit($savepoint = null)
    {
        return $this->db->commit($savepoint);
    }

    /**
     * 回滾交易
     * Rollback changes in a transaction.
     *
     * @param string|null $savepoint
     * @return void
     */
    public function rollback($savepoint = null)
    {
        return $this->db->rollback($savepoint);
    }
}