<?php
/**
 * SimpleDB介面
 */
namespace App\Core\Common;

/**
 * Interface SimpleDB, 用來方便存取資料庫的介面
 */
interface SimpleDB
{
    /**
     * Read data by primary key
     */
    public function getDataByKey($keyName, $keyValue);
    
    /**
     * Read data by multi-key
     */    
    public function getDataByMultiKeys(array $keyValuePairs);

    /**
     * Read multiple rows by key
     */
    public function getRowsByKey($keyName, $keyValue);

    /**
     * Read multiple rows by multi-key
     */
    public function getRowsByMultiKeys(array $keyValuePairs);

    /**
     * Insert data
     */
    public function insertData(Array $data);

    /**
     * Update data by primary key
     */
    public function updateDataByKey(Array $data, $keyName, $keyValue);

    /**
     * Delete data by primary key
     */
    public function deleteDataByKey($keyName, $keyValue);

    /**
     * Update data by multi-keys
     */       
    public function updateDataByMultiKeys(array $data, array $keyValuePairs);

    /**
     * Delete data by multi-keys
     */      
    public function deleteDataByMultiKeys(array $keyValuePairs);
}
