<?php

namespace App\Core\Common;

/**
 * Class BaseService: Implement __call method
 */
class BaseService
{
    protected $resultObj;

    public function __construct(){
        $this->resultObj = new ApiResultImpl();
    }

    public function __call($method, $arguments) {
        if(!method_exists($this, $method)) {
            throw new \Exception($method . ' not fould in Class' . get_class($this));
        }
        try {
            return call_user_func_array(array($this, $method), $arguments);
        }catch (BaseException $e) {
            $this->resultObj->setFail($e->getCode(), $e->getPayload(), $e->getShowMessage(), (string)$e, $e->getFormatMessage());
        }catch (\RuntimeException $e) {
            $this->resultObj->setServerFail(null, '', $e->getMessage());
        }catch (\Exception $e) {
            $this->resultObj->setServerFail(null, '', $e->getMessage());
        }
        return $this->resultObj->apiResult;
    }
}