<?php
/**
 * API Result Interface
 */

namespace App\Core\Common;

/**
 * API Result Interface
 */
interface ApiResultInterface
{
    /**
     * 初值化API回傳結構
     * code=100
     */
    public function init();

    /**
     * 設定API回傳狀態為成功
     * code: 結果代碼 (此處一律為200)
     *
     * @param array|null $payload: 回傳內容,若給null會將payload清掉(即unset)
     * @param string|null $sMessage: show message之意，供前端使用，為 html文字，可直接呈現的訊息
     * @param string|null $eMessage: error message之意，供後端debug用，為exception錯誤內容，當前端使用API有不解的錯誤，可以此給後端trace
     * @param string|null $fMessage: format message之意，供前端轉譯使用
     * @param mixed $debug 除錯資訊
     * @return array apiResult
     */
    public function setSuccess($payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null);

    /**
     * 設定API回傳狀態為錯誤
     *
     * @param int $code: 結果代碼，目前定義了 400/500 (client/server)
     * @param array|null $payload: 回傳內容,若給null會將payload清掉(即unset)
     * @param string|null $sMessage: show message之意，供前端使用，為 html文字，可直接呈現的訊息
     * @param string|null $eMessage: error message之意，供後端debug用，為exception錯誤內容，當前端使用API有不解的錯誤，可以此給後端trace
     * @param string|null $fMessage: format message之意，供前端轉譯使用
     * @param mixed $debug 除錯資訊
     * @return array apiResult
     */
    public function setFail(int $code, $payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null);

    /**
     * 用指定的Key值來設定apiResult的payload中的值
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function setPayloadByKey($key, $value);

    /**
     * 設定api回傳結果
     *
     * @param boolean $isSuccess
     * @param mixed $payload
     * @param string|null $message
     * @param integer $code 結果代碼
     * @param mixed $debug
     * @return void
     */
    public function setApiResult(bool $isSuccess, int $code, $payload=null, $sMessage='', $eMessage=null, $fMessage=null, $debug=null);
}