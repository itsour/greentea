<?php 
namespace App\Core\Mailer;

use App\Core\Config\Config;
use PHPMailer\PHPMailer\PHPMailer;

/**
* PHPMailer Adapt Class
*/
class Mailer
{
    /**
     * The application instance extends from PHPMailer.
     *
     * @var \PHPMailer
     */
    private static $_uniqueInstance;
    
    /**
     * create an extended PHPMailer instance
     * @return object  static
     */
    public static function getInstance($vendor = 'main')
    {
        if (null !== static::$_uniqueInstance) {
            return static::$_uniqueInstance;
        }

        $env = Config::_getEnv('mailer');

        $mailer = new PHPMailer(true);
        if ($env[$vendor]['adapter'] == 'smtp') {
            // Set mailer to use SMTP
            $mailer->isSMTP();
        }
        
        $mailer->Host     = $env[$vendor]['host'];
        $mailer->CharSet  = $env[$vendor]['charset'];
        $mailer->Encoding = $env[$vendor]['encoding'];
        $mailer->Port     = $env[$vendor]['port'];

        if (self::isConfigSet($env, $vendor, 'enable_auth')) {
            $mailer->SMTPAuth = $env[$vendor]['enable_auth']; //SMTP authentication
        }
        if (self::isConfigSet($env, $vendor, 'user')) {
            $mailer->Username = $env[$vendor]['user'];
        }
        if (self::isConfigSet($env, $vendor, 'pass')) {
            $mailer->Password = $env[$vendor]['pass']; 
        }
        if (self::isConfigSet($env, $vendor, 'encryption')) {
            $mailer->SMTPSecure = $env[$vendor]['encryption'];
        }
        
        $mailer->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        return static::$_uniqueInstance = $mailer;
    }

    private static function isConfigSet($env, $vendor, $setting)
    {
        //沒設或是空值都是false
        return (($env[$vendor][$setting] ?? null) == null) ? false : true;
    }
}