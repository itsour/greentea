<?php
/**
 * Class FileLogger LogAdapter實作
 */

namespace App\Core\Logger;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\RotatingFileHandler;
use App\Core\Config\Config;
use App\Utility\Utility;

/**
 * 透過Monolog實作方便的檔案Log機制
 */
class FileLogger implements LogAdapter
{
    private static $_uniqueInstance;
    protected static $defaultLogPath =  __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'storages';
    protected static $defaultLogFileName = 'bp_log.log';
    protected static $defaultLogChannel = 'bp_log';
    protected static $defaultIsRotate = true;
    protected static $defaultRotateMaxFiles = 0;
    protected static $defaultPermission = 0666;

    /**
     * 寫入emergency等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function emergency($message, $context=array(), $fileName='', $channel='')
    {
        $logger = self::prepareMonologger($fileName, $channel, Logger::EMERGENCY);
        $logger->emergency($message, $context);
    }

    /**
     * 寫入alert等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function alert($message, $context=array(), $fileName='', $channel='')
    {
        $logger = self::prepareMonologger($fileName, $channel, Logger::ALERT);
        $logger->alert($message, $context);
    }

    /**
     * 寫入critical等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function critical($message, $context=array(), $fileName='', $channel='')
    {
        $logger = self::prepareMonologger($fileName, $channel, Logger::CRITICAL);
        $logger->critical($message, $context);
    }
    
    /**
     * 寫入error等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */ 
    public static function error($message, $context=array(), $fileName='', $channel='')
    {
        $logger = self::prepareMonologger($fileName, $channel, Logger::ERROR);
        $logger->error($message, $context);
    }

    /**
     * 寫入warning等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function warning($message, $context=array(), $fileName='', $channel='')
    {
        $logger = self::prepareMonologger($fileName, $channel, Logger::WARNING);
        $logger->warning($message, $context);
    }

    /**
     * 寫入notice等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function notice($message, $context=array(), $fileName='', $channel='')
    {
        $logger = self::prepareMonologger($fileName, $channel, Logger::NOTICE);
        $logger->notice($message, $context);
    }

    /**
     * 寫入info等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function info($message, $context=array(), $fileName='', $channel='')
    {
        $logger = self::prepareMonologger($fileName, $channel, Logger::INFO);
        $logger->info($message, $context);
    }

    /**
     * 寫入debug等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function debug($message, $context=array(), $fileName='', $channel='')
    {
        $logger = self::prepareMonologger($fileName, $channel, Logger::DEBUG);
        $logger->debug($message, $context);
    }

    /**
     * 取得Monolog的Logger物件
     *
     * @param string $fileName
     * @param string $channel
     * @param string $logLevel
     * @return LoggerInterface
     */
    private static function prepareMonologger($fileName='', $channel='', $logLevel='')
    {
        $config = new Config();
        $fullFilePath = self::getDefaultFullLogFileName($fileName, $config);

        if ($channel == '') {
            $channel = self::$defaultLogChannel;
        }
        if ($logLevel == '') {
            $logLevel = Logger::DEBUG;
        }
        
        $isRotate = $config('system.file_log.is_rotate') ?? self::$defaultIsRotate;
        $rotateMaxFiles = $config('system.file_log.rotate_max_files') ?? self::$defaultRotateMaxFiles;

        if ($isRotate) {
            $stream = new RotatingFileHandler($fullFilePath, $rotateMaxFiles, $logLevel, true, self::$defaultPermission);
        } else {
            $stream = new StreamHandler($fullFilePath, $logLevel, true, self::$defaultPermission);
        }
        
        $logger = new Logger($channel);
        $logger->pushHandler($stream);
        return $logger;
    }

    private static function getDefaultFullLogFileName($fileName, $config)
    {
        $logFolder = $config('system.file_log.log_folder_path');
        //config的log_folder_path設定值支援array格式(會自動轉換)或字串格式
        $logFolder = Utility::convertFilePathArrayToString($logFolder, self::$defaultLogPath);

        if ($fileName == '') {
            $fileName = self::$defaultLogFileName;
        }

        $fullFilePath = $logFolder . DIRECTORY_SEPARATOR . $fileName;
        return $fullFilePath;
    }
}