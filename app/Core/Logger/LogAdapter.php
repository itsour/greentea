<?php
/**
 * Interface LogAdapter
 */

namespace App\Core\Logger;

/**
 * Log轉接器介面
 */
interface LogAdapter
{
    /**
     * 寫入emergency等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function emergency($message, $context=array(), $fileName='', $channel='');

    /**
     * 寫入alert等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function alert($message, $context=array(), $fileName='', $channel='');

    /**
     * 寫入critical等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function critical($message, $context=array(), $fileName='', $channel='');

    /**
     * 寫入error等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */    
    public static function error($message, $context=array(), $fileName='', $channel='');

    /**
     * 寫入warning等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function warning($message, $context=array(), $fileName='', $channel='');

    /**
     * 寫入notice等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function notice($message, $context=array(), $fileName='', $channel='');

    /**
     * 寫入info等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function info($message, $context=array(), $fileName='', $channel='');

    /**
     * 寫入debug等級之log
     *
     * @param string $message 紀錄的文字訊息
     * @param array $context 附加在log中的內容
     * @param string $fileName log檔名(可不指定)
     * @param string $channel 可於log中辨識此筆log的來源(若不指定則用預設值)
     */
    public static function debug($message, $context=array(), $fileName='', $channel='');
}