<?php 
/**
 * 資料庫連線介面,用在BaseModel class
 */
namespace App\Core\Db;

/**
 * DbConnection interface
 */
interface DbConnection
{
    /**
     * Get DB vendor instance
     *
     * @param string $vendor DB vendor name in config, default is "main"
     * @return object DB connect object
     * @throws Exception if the connection fails.
     */
    public static function getInstance($vendor='main');
}