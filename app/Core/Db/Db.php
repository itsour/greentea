<?php 
/**
 * 以Dibi實作DbConnection
 */
namespace App\Core\Db;

use App\Core\Config\Config;
use dibi;

/**
* DB API 
* return DB API instance of dibi package
*/
class Db implements DbConnection
{
    /**
     * Keep multiple vendor database connection instance
     * 
     * @var array assoc-array, key: vendor name, value: object of Dibi\Connection
     */
    private static $_multiDbInstance;

    /**
     * Get DB vendor instance, each vendor connection is unique (singleton pattern)
     *
     * @param string $vendor DB vendor name in config, default is "main"
     * @return object DB connect object
     * @throws \Dibi\Exception if the connection fails.
     */
    public static function getInstance($vendor = 'main') 
    {
        $env = Config::_getEnv('database');
        if (!isset($env[$vendor])) {
            $appEnv = Config::getAppEnv();
            throw new \RuntimeException('Can not find "'. $vendor . '" vendor setting in database config of env "' . $appEnv . '"');
        }

        $connSetting = array(
            'driver'       => $env[$vendor]['adapter'],
            'host'         => $env[$vendor]['host'],
            'database'     => $env[$vendor]['name'],
            'username'     => $env[$vendor]['user'],
            'password'     => $env[$vendor]['pass'],
            'port'         => $env[$vendor]['port'],
        );

        //support charset, especially MySql utf8mb4
        if (isset($env[$vendor]['charset'])) {
            $connSetting['charset'] = $env[$vendor]['charset'];
        }
        //support result for response format setting
        if (isset($env[$vendor]['result']) && is_array($env[$vendor]['result'])) {
            $connSetting['result'] = $env[$vendor]['result'];
        }
        //support profiler
        if (isset($env[$vendor]['profiler']) && is_array($env[$vendor]['profiler'])) {
            $connSetting['profiler'] = $env[$vendor]['profiler'];
        }
        //support lazy connect
        if (!empty($env[$vendor]['lazy'])) {
            $connSetting['lazy'] = $env[$vendor]['lazy'];
        }

        if (!is_object(self::$_multiDbInstance[$vendor] ?? null)) {
            self::$_multiDbInstance[$vendor] = dibi::connect($connSetting);
        }

        return self::$_multiDbInstance[$vendor];
    }
}