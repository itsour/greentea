<?php 
/**
 * Define database opeartion interface
 */
namespace App\Core\Db;

/**
 * 資料庫操作介面，用來定義資料庫物件可使用的操作行為
 */
interface DbOperateInterface
{
    /**
     * 建立並執行SQL語句
     * Generates and executes SQL query.
     * **SQL中的變數必須要使用modifier以防SQL Injection**
     * 
     * @param mixed ...$args SQL語句與變數
     * @return mixed 回傳值依所執行的語句而有所不同
     */
    public function query(...$args);

    /**
     * 依SQL取得一筆資料
     * Fetches the row at current position, process optional type conversion 
     * and moves the internal cursor to the next position
     *
     * @param mixed ...$args SQL語句與變數
     * @return mixed 單筆資料的物件，可能為null
     */
    public function fetch(...$args);

    /**
     * 依SQL取得全部的紀錄
     * Fetches all records from table.
     *
     * @param mixed ...$args SQL語句與變數
     * @return array
     */
    public function fetchAll(...$args): array;

    /**
     * 類似fetch，但只回傳第一個欄位的內容
     * Like fetch(), but returns only first field.
     *
     * @param mixed ...$args SQL語句與變數
     * @return mixed 該欄位的值，可能為null
     */
    public function fetchSingle(...$args);

    /**
     * 依SQL取得以key => value為一筆資料的兩欄位鍵值組合
     * Fetches all records from table like $key => $value pairs.
     *
     * @param mixed ...$args SQL語句與變數
     * @return array
     */
    public function fetchPairs(...$args): array;

    /**
     * 依SQL取得資料，並依assoc的設定將結果組成關聯式陣列結構
     * Fetches all records from table and returns associative tree.
     *
     * @param string $assoc assoc的設定
     * @param mixed ...$args SQL語句與變數
     * @return array
     */
    public function fetchAssoc($assoc, ...$args): array;

    /**
     * 交易開始
     * Begins a transaction (if supported).
     *
     * @param string|null $savepoint
     */
    public function begin($savepoint = null);

    /**
     * 簽入交易
     * Commits statements in a transaction.
     *
     * @param string|null $savepoint 
     */
    public function commit($savepoint = null);

    /**
     * 回滾交易
     * Rollback changes in a transaction.
     *
     * @param string|null $savepoint 
     */
    public function rollback($savepoint = null);
}