<?php 
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use App\Core\Config\Config;
use App\RouteMiddleware\CorsMiddleware;
use App\Core\Common\Views;
use App\Core\Debug\Debug;
use App\RouteMiddleware\Mark2MiddleLayering;
use Slim\Factory\AppFactory;

require_once __DIR__ . "/../vendor/autoload.php";

// 解決Cookie No HttpOnly Flag
ini_set('session.cookie_httponly', 1);
// debug object for MarkII
$bpDebug = new Debug();

// Create Slim app object
$app = AppFactory::create();

// Add middlewares for MarkII
$mwLayer = new Mark2MiddleLayering($bpDebug);
$mwLayer->layerMiddlewares($app);

// Add default routes
$app->redirect('/', '/index', 302);

$app->get('/index', function (Request $request, Response $response, array $args) {
    $views = Views::getInstance();
    $content = $views->render('public/index');
    $response->getBody()->write($content);
    return $response;
});

// Auto load project routes
$files = glob(__DIR__ . '/../routes/public/' . '*.php');
foreach ($files as $file) {
    require_once($file);
}

$app->run();
